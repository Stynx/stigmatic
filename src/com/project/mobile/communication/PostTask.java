package com.project.mobile.communication;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import com.project.mobile.communication.model.HttpRequestModel;
import com.project.mobile.communication.model.HttpResponseModel;
import com.project.mobile.utility.Logger;

/**
 * 
 * Task for REST POST operations
 * 
 */
public class PostTask extends RestTask {

	private HttpRequestModel httpRequestModel;
	private IRestTaskCallback iRestTaskCallback;

	public PostTask(HttpRequestModel httpRequestModel, IRestTaskCallback iRestTaskCallback) {

		this.httpRequestModel = httpRequestModel;
		this.iRestTaskCallback = iRestTaskCallback;

	}

	@Override
	protected HttpResponseModel doInBackground(String... params) {

		HttpResponseModel responseModel = new HttpResponseModel();

		// default status
		responseModel.setStatus(HttpResponseModel.STATUS_ERROR);

		final HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, CONNECTION_TIMEOUT);
		HttpConnectionParams.setSoTimeout(httpParams, PROGRESS_TIMEOUT);

		HttpClient httpClient = new DefaultHttpClient(httpParams);
		HttpPost httpost = new HttpPost(httpRequestModel.getRestUrl());

		try {

			// executing request here
			HttpResponse response = httpClient.execute(httpost);

			if (response != null) {

				// If status is OK 200
				if (response.getStatusLine().getStatusCode() == 200) {

					responseModel.setStatus(HttpResponseModel.STATUS_SUCCESS);

					responseModel.setResponse(response.getEntity().getContent());

				}

			}

			return responseModel;

		} catch (Exception e) {

			Logger.log(Logger.TYPE_ERROR, e.getMessage(), getClass().getName());
			responseModel.setMessage(e.getMessage());
			iRestTaskCallback.onTaskComplete(responseModel);
			return responseModel;

		}

	}

	@Override
	protected void onPostExecute(HttpResponseModel response) {
		
		// execute callback method here
		iRestTaskCallback.onTaskComplete(response);
		super.onPostExecute(response);

	}

}

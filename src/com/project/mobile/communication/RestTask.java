package com.project.mobile.communication;

import android.os.AsyncTask;

import com.project.mobile.communication.model.HttpResponseModel;

/**
 * 
 * Base task for REST operations
 * 
 */
public abstract class RestTask extends AsyncTask<String, Integer, HttpResponseModel> {

	protected String restUrl; // Url for rest operations
	protected IRestTaskCallback iRestTaskCallback; // main callback for rest
	protected final int CONNECTION_TIMEOUT = 10000; // first connection timeout
	protected final int PROGRESS_TIMEOUT = 30000; // progress timeout

}

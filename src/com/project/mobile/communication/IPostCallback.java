package com.project.mobile.communication;

/**
 * 
 * Interface for a callback to be invoked when the response for the data submission is available
 * 
 */
public interface IPostCallback {

	/**
	 * Called when a POST success response is received. This method is guaranteed to execute on the UI thread
	 */
	public void onPostSuccess();

}

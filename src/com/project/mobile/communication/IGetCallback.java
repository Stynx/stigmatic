package com.project.mobile.communication;

import com.project.mobile.communication.model.HttpResponseModel;

/**
 * Interface for a callback to be invoked when the response data for the GET call is available
 */
public interface IGetCallback {

	/**
	 * Called when the response data for the REST call is ready. This method is guaranteed to execute on the UI thread
	 * 
	 * @param response
	 */
	public void onDataReceived(HttpResponseModel response);

}

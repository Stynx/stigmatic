package com.project.mobile.communication;

import com.project.mobile.communication.model.HttpResponseModel;

/**
 * Low-level layer callback object
 * 
 */
public interface IRestTaskCallback {

	/**
	 * Called when the HTTP request completess
	 * 
	 * @param responseModel
	 */
	void onTaskComplete(HttpResponseModel responseModel);

}

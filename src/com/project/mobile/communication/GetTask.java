package com.project.mobile.communication;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import com.project.mobile.communication.model.HttpResponseModel;
import com.project.mobile.utility.Logger;

/**
 * 
 * Task for REST GET operations
 * 
 */
public class GetTask extends RestTask {

	public GetTask(String restUrl, IRestTaskCallback iRestTaskCallback) {

		this.restUrl = restUrl;
		this.iRestTaskCallback = iRestTaskCallback;

	}

	@Override
	protected HttpResponseModel doInBackground(String... params) {

		HttpResponseModel responseModel = new HttpResponseModel();
		responseModel.setStatus(HttpResponseModel.STATUS_ERROR);

		// HTTP GET
		try {

			// set the connection timeout value to 30 seconds (30000 milliseconds)
			final HttpParams httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParams, CONNECTION_TIMEOUT);
			HttpConnectionParams.setSoTimeout(httpParams, PROGRESS_TIMEOUT);

			// create HttpClient
			HttpClient httpclient = new DefaultHttpClient(httpParams);

			HttpGet httpGet = new HttpGet(restUrl);

			// make GET request to the given URL
			HttpResponse response = httpclient.execute(httpGet);

			if (response != null) {

				// If status is OK 200
				if (response.getStatusLine().getStatusCode() == 200) {

					responseModel.setStatus(HttpResponseModel.STATUS_SUCCESS);

					// receive response as inputStream
					responseModel.setResponse(response.getEntity().getContent());

				}

			}

			return responseModel;

		} catch (Exception e) {

			Logger.log(Logger.TYPE_ERROR, e.getMessage(), getClass().getName());

			responseModel.setMessage(e.getMessage());
			iRestTaskCallback.onTaskComplete(responseModel);

			return responseModel;

		}

	}

	@Override
	protected void onPostExecute(HttpResponseModel response) {

		// execute callback method here
		iRestTaskCallback.onTaskComplete(response);
		super.onPostExecute(response);

	}

}

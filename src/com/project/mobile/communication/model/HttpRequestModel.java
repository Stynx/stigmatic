package com.project.mobile.communication.model;

/**
 * 
 * Simple HttpRequestModel for REST operations
 * 
 */
public class HttpRequestModel {

	private String restUrl;
	private String requestBody;

	public HttpRequestModel() {

	}

	public HttpRequestModel(String restUrl) {

		this.restUrl = restUrl;

	}

	public HttpRequestModel(String restUrl, String requestBody) {

		this.restUrl = restUrl;
		this.requestBody = requestBody;

	}

	public String getRestUrl() {

		return restUrl;

	}

	public void setRestUrl(String restUrl) {

		this.restUrl = restUrl;

	}

	public String getRequestBody() {

		return requestBody;

	}

	public void setRequestBody(String requestBody) {

		this.requestBody = requestBody;

	}

}

package com.project.mobile.communication.model;

import java.io.BufferedInputStream;
import java.io.InputStream;

/**
 * 
 * Simple HttpResponseModel for REST operations
 * 
 */
public class HttpResponseModel {

	// success status code
	public final static int STATUS_SUCCESS = 0;

	// error status code
	public final static int STATUS_ERROR = 1;

	// timeout status code
	public final static int STATUS_TIMEOUT = 2;

	// status for HttpUrlConnection result
	private int status;

	// response created after a sucessful HttpUrlConnection will be used in
	// XmlPullParser
	private InputStream response;

	// message if any error happens
	private String message;

	public HttpResponseModel() {

	}

	public HttpResponseModel(int status, BufferedInputStream response) {

		this.status = status;
		this.response = response;

	}

	public HttpResponseModel(int status, BufferedInputStream response, String message) {

		this.status = status;
		this.response = response;
		this.message = message;

	}

	public boolean getStatusBoolean() {

		if (status == STATUS_SUCCESS) {

			return true;

		}

		return false;

	}

	public int getStatus() {

		return status;

	}

	public void setStatus(int status) {

		this.status = status;

	}

	public InputStream getResponse() {

		return response;

	}

	public void setResponse(InputStream response) {

		this.response = response;

	}

	public String getMessage() {

		return message;

	}

	public void setMessage(String message) {

		this.message = message;

	}

}

package com.project.mobile.utility;

import android.util.Log;

import com.project.stigmatic.BuildConfig;

/**
 * 
 * Centralized logger class for application.
 * 
 */
public abstract class Logger {

	public final static int TYPE_ERROR = 0;
	public final static int TYPE_INFO = 1;
	public final static int TYPE_WARN = 2;

	private final static String TAG_ERROR = "ERROR";
	private final static String TAG_INFO = "INFO";
	private final static String TAG_WARN = "WARNING";

	private final static String SOURCE = "Source:";
	private final static String MESSAGE = "Message:";

	/**
	 * Logger method with source and message
	 * 
	 * @param type
	 *            Error, Warning or Info
	 * @param text
	 *            Message
	 * @param className
	 *            Class that sends log
	 */
	public static void log(int type, String text, String className) {

		// if application is deployed to market, this parameter will prevent logs.
		if (BuildConfig.DEBUG) {

			if (text == null && className == null) {

				Log.e(TAG_ERROR, "Unidentified Error Happened..");
				return;

			}

			StringBuffer msgBuffer = new StringBuffer("");

			if (className != null && !className.isEmpty()) {

				msgBuffer.append(SOURCE);
				msgBuffer.append(" ");
				msgBuffer.append(className);
				msgBuffer.append("\n");

			}

			if (text != null && !text.isEmpty()) {

				msgBuffer.append(MESSAGE);
				msgBuffer.append(" ");
				msgBuffer.append(text);

			}

			String messageString = msgBuffer.toString();

			switch (type) {

			case TYPE_ERROR:
				Log.e(TAG_ERROR, messageString);
				break;
			case TYPE_INFO:
				Log.i(TAG_INFO, messageString);
				break;
			default:
				Log.w(TAG_WARN, messageString);
				break;

			}

		}

	}

}

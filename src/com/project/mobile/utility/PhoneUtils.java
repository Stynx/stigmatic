package com.project.mobile.utility;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import android.content.ContentUris;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;

/**
 * 
 * Class that has device utility functions
 * 
 */
public abstract class PhoneUtils {

	/**
	 * Network check. Still needs to ping the ip but this is first layer.
	 * 
	 * @return
	 */
	public static boolean hasNetworkConnection(Context context) {

		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();

		if (netInfo != null && netInfo.isConnected()) {

			return true;

		}

		return false;

	}

	/**
	 * Open contact photo and returns it as byte array
	 * 
	 * @param contactId
	 *            Id of the user who the photo belongs to
	 * @param c
	 *            Current context
	 * @return
	 */
	public static byte[] openContactPhoto(long contactId, Context c) {

		// Contacts full size photo
		Uri contactUri = ContentUris.withAppendedId(Contacts.CONTENT_URI, contactId);
		Uri displayPhotoUri = Uri.withAppendedPath(contactUri, Contacts.Photo.DISPLAY_PHOTO);

		try {

			AssetFileDescriptor fd = c.getContentResolver().openAssetFileDescriptor(displayPhotoUri, "r");
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			int read = 0;
			byte[] buffer = new byte[1024];

			while ((read = fd.createInputStream().read(buffer, 0, buffer.length)) != -1) {

				output.write(buffer, 0, read);

			}

			byte[] photoArray = output.toByteArray();

			return photoArray;

		} catch (IOException e) {

			return null;

		}

	}

}

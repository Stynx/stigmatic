package com.project.mobile.utility;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;

/**
 * 
 * Simple date utility methods defined here
 * 
 */
@SuppressLint("SimpleDateFormat")
public abstract class DateUtils {

	/**
	 * Converts specified timestamp to string based on given format
	 * 
	 * @param format
	 *            Date format given
	 * @param timestamp
	 * @return
	 */
	public static String getDateStringFromTimestamp(String format, long timestamp) {

		String dateStr = "";

		try {

			Date date = new Date(timestamp);
			dateStr = new SimpleDateFormat(format).format(date);

		} catch (IllegalArgumentException ex) {

			Logger.log(Logger.TYPE_ERROR, ex.getMessage(), DateUtils.class.getName());

		} catch (NullPointerException ex) {

			Logger.log(Logger.TYPE_ERROR, ex.getMessage(), DateUtils.class.getName());

		}

		return dateStr;

	}

}

package com.project.mobile.utility;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

/**
 * 
 * Basic conversion functions defined here
 * 
 */
public abstract class ConvertUtils {

	/**
	 * Converts InputStream to String
	 * 
	 * @param is
	 * @return
	 */
	public static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;

		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {

				sb.append(line);

			}

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			if (br != null) {

				try {

					br.close();

				} catch (IOException e) {

					e.printStackTrace();

				}

			}

		}

		return sb.toString();

	}

	/**
	 * Converts byte array to bitmap image
	 * 
	 * @param byteArray
	 * @return
	 */
	public static Bitmap getBitmapFromByteArray(byte[] byteArray) {

		return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

	}

	/**
	 * Converts input stream to byte array
	 * 
	 * @param byteArray
	 * @return
	 */
	public static byte[] getByteArrayFromInputStream(InputStream input) throws IOException {

		byte[] buffer = new byte[8192];
		int bytesRead;
		ByteArrayOutputStream output = new ByteArrayOutputStream();

		while ((bytesRead = input.read(buffer)) != -1) {

			output.write(buffer, 0, bytesRead);

		}

		return output.toByteArray();

	}

	/**
	 * Converts bitmap to Base64 string
	 * 
	 * @param bmp
	 * @param format
	 * @param quality
	 * @return
	 */
	public static String getBase64StringFromBitmap(Bitmap bmp) {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] bytes = baos.toByteArray();
		return Base64.encodeToString(bytes, Base64.DEFAULT);

	}

}
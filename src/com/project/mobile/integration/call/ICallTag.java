package com.project.mobile.integration.call;

public class ICallTag {

	/**
	 * Call Log General Fields
	 */
	public static final String TEL = "tel";
	public static final String COLON = ":";
	public static final String MISSED_CALL = "Missed Call";

}

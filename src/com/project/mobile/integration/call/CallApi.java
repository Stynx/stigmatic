package com.project.mobile.integration.call;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.CallLog.Calls;
import android.provider.ContactsContract.PhoneLookup;

import com.project.mobile.utility.DateUtils;
import com.project.mobile.utility.PhoneUtils;
import com.project.stigmatic.configuration.ApplicationSettings;
import com.project.stigmatic.model.StigmaticModel;

/**
 * 
 * This class retrieves phone calls from the phone. "<uses-permission android:name="android.permission.READ_CALL_LOG" />" is required in the
 * manifest.
 * 
 */
public class CallApi {

	// singleton instance
	private static final CallApi INSTANCE = new CallApi();

	private String dateFormat;

	private CallApi() {

	}

	public static CallApi getInstance() {

		return INSTANCE;

	}

	/**
	 * 
	 * @param c
	 *            Current context
	 * @return Returns phone call list that contains wrapper models in it
	 */
	public List<StigmaticModel> getCallList(Context c, Long lastTimestamp) {

		ArrayList<StigmaticModel> callList = new ArrayList<StigmaticModel>();

		Cursor callCursor = c.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, "date > ?", new String[] { String.valueOf(lastTimestamp) }, null);
		StigmaticModel callItem = null;

		String contactName = "";
		String contactPhoneNumber = "";

		// if date format specified before calling this method, it is used.
		String dateFormat = this.dateFormat != null ? this.dateFormat : "dd.MM.yyyy HH:mm";

		Cursor contactCursor = null;

		try {

			// parsing calls with iterator
			while (callCursor.moveToNext()) {

				callItem = new StigmaticModel();

				if (Integer.parseInt(callCursor.getString(callCursor.getColumnIndex(Calls.TYPE))) == Calls.MISSED_TYPE) {

					// Get contact display name and id
					ContentResolver resolver = c.getContentResolver();
					Uri lookupUri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI,
							Uri.encode(callCursor.getString(callCursor.getColumnIndex(CallLog.Calls.NUMBER)).replace("+", "")));

					contactCursor = resolver.query(lookupUri, new String[] { PhoneLookup.DISPLAY_NAME, PhoneLookup._ID }, null, null, null);
					try {
						contactCursor.moveToFirst();
					} catch (Exception e) {

					}
					contactName = callCursor.getString(callCursor.getColumnIndex(CallLog.Calls.NUMBER));
					contactPhoneNumber = contactName;

					try {

						contactName = contactCursor.getString(0);

					} catch (Exception e) {

					}

					try {

						int contactID = Integer.parseInt(contactCursor.getString(1));

						// Get contact photo
						if (contactID != 0) {

							callItem.setPhoto(PhoneUtils.openContactPhoto(contactID, c));

						}

					} catch (Exception e) {

						callItem.setPhoto(null);

					}

					long timestamp = Long.parseLong(callCursor.getString(callCursor.getColumnIndex(Calls.DATE)));

					String dateAsString = DateUtils.getDateStringFromTimestamp(dateFormat, timestamp);

					// Set timestamp
					callItem.setTimestamp(timestamp);
					// Set sms date
					callItem.setDate(dateAsString);
					// Set contact name
					callItem.setUser(contactName);
					// Set sms body
					callItem.setMessage(ICallTag.MISSED_CALL);
					// Set platform
					callItem.setPlatform(ApplicationSettings.PLATFORM_CALL);

					callItem.setLink(ICallTag.TEL.concat(ICallTag.COLON).concat(contactPhoneNumber));

					// Intent callIntent = new Intent(Intent.ACTION_CALL);
					// callIntent.setData(Uri.parse("tel:123456789"));
					// startActivity(callIntent);

					callList.add(callItem);

				}

				// closes cursors to prevent memory leak
				if (contactCursor != null)
					contactCursor.close();

			}

		} finally {

			if (callCursor != null)
				callCursor.close();

		}

		return callList;

	}

	public void setDateFormat(String dateFormat) {

		this.dateFormat = dateFormat;

	}

}

package com.project.mobile.integration.linkedin.model;

public class PersonModel {

	private String firstName;
	private String lastName;
	private String headLine;
	private String profileUrl;
	private String pictureUrl;

	public String getFirstName() {

		if (firstName == null)
			return "";

		return firstName;

	}

	public void setFirstName(String firstName) {

		this.firstName = firstName;

	}

	public String getLastName() {

		if (lastName == null)
			return "";

		return lastName;

	}

	public void setLastName(String lastName) {

		this.lastName = lastName;

	}

	public String getHeadLine() {

		if (headLine == null)
			return "";

		return headLine;

	}

	public void setHeadLine(String headLine) {

		this.headLine = headLine;

	}

	public String getProfileUrl() {

		return profileUrl;

	}

	public void setProfileUrl(String profileUrl) {

		this.profileUrl = profileUrl;

	}

	public String getPictureUrl() {

		return pictureUrl;

	}

	public void setPictureUrl(String pictureUrl) {

		this.pictureUrl = pictureUrl;

	}

}

package com.project.mobile.integration.linkedin.model;

public abstract class NetworkUpdateModel {

	protected String updateType;
	protected String updateUrl;
	protected String updateKey;
	protected long timestamp;

	public String getUpdateType() {

		return updateType;

	}

	public void setUpdateType(String updateType) {

		this.updateType = updateType;

	}

	public String getUpdateUrl() {

		return updateUrl;

	}

	public void setUpdateUrl(String updateUrl) {

		this.updateUrl = updateUrl;

	}

	public long getTimestamp() {

		return timestamp;

	}

	public void setTimestamp(long timestamp) {

		this.timestamp = timestamp;

	}

	public String getUpdateKey() {

		return updateKey;

	}

	public void setUpdateKey(String updateKey) {

		this.updateKey = updateKey;

	}

}

package com.project.mobile.integration.linkedin.model;

/**
 * 
 * Update Type: CMPY
 * 
 */
public class CompanyUpdateModel extends NetworkUpdateModel {

	private int id; // company id
	private String name; // company name
	private String action;
	private String comment;
	private String thumbnailUrl;
	private String link;

	public String getName() {

		return name;

	}

	public void setName(String name) {

		this.name = name;

	}

	public String getAction() {

		return action;

	}

	public void setAction(String action) {

		this.action = action;

	}

	public int getId() {

		return id;

	}

	public void setId(int id) {

		this.id = id;

	}

	public String getComment() {

		return comment;

	}

	public void setComment(String comment) {

		this.comment = comment;

	}

	public String getThumbnailUrl() {

		return thumbnailUrl;

	}

	public void setThumbnailUrl(String thumbnailUrl) {

		this.thumbnailUrl = thumbnailUrl;

	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

}

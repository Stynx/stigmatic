package com.project.mobile.integration.linkedin.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Update Type: CONN - Example String: "John Irving is now connected to Paul Auster."
 * 
 */
public class ConnectionUpdateModel extends NetworkUpdateModel {

	private PersonModel person;
	private List<PersonModel> newConnectionList;

	public PersonModel getPerson() {

		return person;

	}

	public void setPerson(PersonModel person) {

		this.person = person;

	}

	public List<PersonModel> getNewConnectionList() {

		if (newConnectionList == null) {

			newConnectionList = new ArrayList<PersonModel>();

		}

		return newConnectionList;

	}

	public void setNewConnectionList(List<PersonModel> newConnectionList) {

		this.newConnectionList = newConnectionList;

	}

}

package com.project.mobile.integration.linkedin;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

import com.project.mobile.integration.IParserCallback;
import com.project.mobile.integration.linkedin.model.CompanyUpdateModel;
import com.project.mobile.integration.linkedin.model.ConnectionUpdateModel;
import com.project.mobile.integration.linkedin.model.NetworkUpdateModel;
import com.project.mobile.integration.linkedin.model.PersonModel;
import com.project.mobile.utility.ConvertUtils;
import com.project.mobile.utility.Logger;

/**
 * 
 * LinkedIn parser. It reads input stream that is sent as json.
 * 
 */
public class LinkedInParser {

	// singleton instance
	private static final LinkedInParser INSTANCE = new LinkedInParser();

	private LinkedInParser() {

	}

	public static LinkedInParser getInstance() {

		return INSTANCE;

	}

	/**
	 * 
	 * LinkedIn network updates are parsed in this class
	 * 
	 */
	private final class LinkedInNetworkUpdateParser extends AsyncTask<InputStream, Integer, List<NetworkUpdateModel>> {

		private IParserCallback<NetworkUpdateModel> parserCallback;

		public LinkedInNetworkUpdateParser(IParserCallback<NetworkUpdateModel> parserCallback) {

			this.parserCallback = parserCallback;

		}

		@Override
		protected List<NetworkUpdateModel> doInBackground(InputStream... params) {

			List<NetworkUpdateModel> updateList = new ArrayList<NetworkUpdateModel>();

			InputStream resultStream = params[0];

			String jsonString = ConvertUtils.getStringFromInputStream(resultStream);

			JSONObject jsonResponse = null;

			try {

				jsonResponse = new JSONObject(jsonString);

			} catch (JSONException e) {

				Logger.log(Logger.TYPE_ERROR, e.getMessage(), getClass().getName());

			}

			if (jsonResponse != null) {

				JSONArray updatesNode = null;
				int updatesLength = 0;

				if (jsonResponse.optJSONArray(ILinkedInTag.VALUES) != null) {

					updatesNode = jsonResponse.optJSONArray(ILinkedInTag.VALUES);
					updatesLength = updatesNode.length();

				}

				JSONObject iteratorNode = null, previousNode = null;
				JSONArray iteratorArray = null;
				String updateType = "", updateKey = null;
				long timestamp = 0;

				PersonModel person = null;
				PersonModel connection = null;
				ConnectionUpdateModel connModel = null;
				CompanyUpdateModel cmpyModel = null;

				for (int i = 0; i < updatesLength; i++) {

					try {

						iteratorNode = updatesNode.getJSONObject(i);

						if (iteratorNode.has(ILinkedInTag.TIMESTAMP))
							timestamp = iteratorNode.getLong(ILinkedInTag.TIMESTAMP);

						if (iteratorNode.has(ILinkedInTag.UPDATE_TYPE))
							updateType = iteratorNode.getString(ILinkedInTag.UPDATE_TYPE);

						if (iteratorNode.has(ILinkedInTag.UPDATE_KEY))
							updateKey = iteratorNode.getString(ILinkedInTag.UPDATE_KEY);

						// connection update type
						if (ILinkedInTag.UPDATE_CONN.equals(updateType)) {

							if (iteratorNode.has(ILinkedInTag.UPDATE_CONTENT)) {

								iteratorNode = iteratorNode.getJSONObject(ILinkedInTag.UPDATE_CONTENT);

								connModel = new ConnectionUpdateModel();
								connModel.setTimestamp(timestamp);
								connModel.setUpdateType(updateType);
								connModel.setUpdateKey(updateKey);

								previousNode = iteratorNode.getJSONObject(ILinkedInTag.PERSON);
								iteratorNode = iteratorNode.getJSONObject(ILinkedInTag.PERSON);
								person = new PersonModel();

								if (iteratorNode.has(ILinkedInTag.FIRST_NAME))
									person.setFirstName(iteratorNode.getString(ILinkedInTag.FIRST_NAME));

								if (iteratorNode.has(ILinkedInTag.LAST_NAME))
									person.setLastName(iteratorNode.getString(ILinkedInTag.LAST_NAME));

								if (iteratorNode.has(ILinkedInTag.HEADLINE))
									person.setHeadLine(iteratorNode.getString(ILinkedInTag.HEADLINE));

								if (iteratorNode.has(ILinkedInTag.PICTURE_URL))
									person.setPictureUrl(iteratorNode.getString(ILinkedInTag.PICTURE_URL));

								if (iteratorNode.has(ILinkedInTag.API_STANDARD_PROFILE_REQUEST)) {

									iteratorNode = iteratorNode.getJSONObject(ILinkedInTag.API_STANDARD_PROFILE_REQUEST);
									if (iteratorNode.has(ILinkedInTag.URL))
										person.setProfileUrl(iteratorNode.getString(ILinkedInTag.URL));

								}

								iteratorNode = previousNode; // back to top

								if (iteratorNode.has(ILinkedInTag.CONNECTIONS)) {

									iteratorNode = iteratorNode.getJSONObject(ILinkedInTag.CONNECTIONS);
									iteratorArray = iteratorNode.getJSONArray(ILinkedInTag.VALUES);

									for (int j = 0; j < iteratorArray.length(); j++) {

										iteratorNode = iteratorArray.getJSONObject(j);
										connection = new PersonModel();

										if (iteratorNode.has(ILinkedInTag.FIRST_NAME))
											connection.setFirstName(iteratorNode.getString(ILinkedInTag.FIRST_NAME));

										if (iteratorNode.has(ILinkedInTag.LAST_NAME))
											connection.setLastName(iteratorNode.getString(ILinkedInTag.LAST_NAME));

										if (iteratorNode.has(ILinkedInTag.HEADLINE))
											connection.setHeadLine(iteratorNode.getString(ILinkedInTag.HEADLINE));

										if (iteratorNode.has(ILinkedInTag.PICTURE_URL))
											connection.setPictureUrl(iteratorNode.getString(ILinkedInTag.PICTURE_URL));

										if (iteratorNode.has(ILinkedInTag.SITE_STANDARD_PROFILE_REQUEST)) {

											iteratorNode = iteratorNode.getJSONObject(ILinkedInTag.SITE_STANDARD_PROFILE_REQUEST);

											if (iteratorNode.has(ILinkedInTag.URL))
												connection.setProfileUrl(iteratorNode.getString(ILinkedInTag.URL));

										}

										connModel.getNewConnectionList().add(connection);

									}

								}

								connModel.setPerson(person);
								updateList.add(connModel);

							}

						} else if (ILinkedInTag.UPDATE_CMPY.equals(updateType)) {

							// company update type

							cmpyModel = new CompanyUpdateModel();
							cmpyModel.setUpdateType(updateType);
							cmpyModel.setTimestamp(timestamp);
							cmpyModel.setUpdateKey(updateKey);

							if (iteratorNode.has(ILinkedInTag.UPDATE_CONTENT)) {

								iteratorNode = iteratorNode.getJSONObject(ILinkedInTag.UPDATE_CONTENT);

								if (iteratorNode.has(ILinkedInTag.COMPANY)) {

									previousNode = iteratorNode;
									iteratorNode = iteratorNode.getJSONObject(ILinkedInTag.COMPANY);

									cmpyModel.setName(iteratorNode.getString(ILinkedInTag.NAME));
									cmpyModel.setId(iteratorNode.getInt(ILinkedInTag.ID));

									iteratorNode = previousNode;

								}

							}

							if (iteratorNode.has(ILinkedInTag.CMPY_STATUS_UPDATE)) {

								iteratorNode = iteratorNode.getJSONObject(ILinkedInTag.CMPY_STATUS_UPDATE);

								if (iteratorNode.has(ILinkedInTag.SHARE)) {

									iteratorNode = iteratorNode.getJSONObject(ILinkedInTag.SHARE);

									if (iteratorNode.has(ILinkedInTag.COMMENT))
										cmpyModel.setComment(iteratorNode.getString(ILinkedInTag.COMMENT));

									if (iteratorNode.has(ILinkedInTag.CONTENT)) {

										iteratorNode = iteratorNode.getJSONObject(ILinkedInTag.CONTENT);

										if (iteratorNode.has(ILinkedInTag.SUBMITTED_URL))
											cmpyModel.setLink(iteratorNode.getString(ILinkedInTag.SUBMITTED_URL));

									}

								}

								updateList.add(cmpyModel);

							} else if (iteratorNode.has(ILinkedInTag.CMPY_JOB_UPDATE)) {

								iteratorNode = iteratorNode.getJSONObject(ILinkedInTag.CMPY_JOB_UPDATE);

								if (iteratorNode.has(ILinkedInTag.JOB)) {

									previousNode = iteratorNode.getJSONObject(ILinkedInTag.JOB);
									iteratorNode = iteratorNode.getJSONObject(ILinkedInTag.JOB);

									if (iteratorNode.has(ILinkedInTag.POSITION)) {

										iteratorNode = iteratorNode.getJSONObject(ILinkedInTag.POSITION);

										if (iteratorNode.has(ILinkedInTag.TITLE)) {

											cmpyModel.setComment(iteratorNode.getString(ILinkedInTag.TITLE));

										}

									}

									iteratorNode = previousNode;

									if (iteratorNode.has(ILinkedInTag.SITE_JOB_REQUEST)) {

										iteratorNode = iteratorNode.getJSONObject(ILinkedInTag.SITE_JOB_REQUEST);

										if (iteratorNode.has(ILinkedInTag.URL)) {

											cmpyModel.setLink(iteratorNode.getString(ILinkedInTag.URL));

										}

									}

								}

								updateList.add(cmpyModel);

							}

						}

					} catch (JSONException ex) {

						Logger.log(Logger.TYPE_ERROR, ex.getMessage(), getClass().getName());

					}

				}

			}

			return updateList;

		}

		@Override
		protected void onPostExecute(List<NetworkUpdateModel> resultList) {

			parserCallback.onParserDataReceived(resultList);
			super.onPostExecute(resultList);

		}

	}

	/**
	 * Gets first degree network updates wrapped in our list and executes the callback class method
	 * 
	 */
	public void getLinkedInNetworkUpdateList(InputStream resultStream, IParserCallback<NetworkUpdateModel> parserCallback) {

		new LinkedInNetworkUpdateParser(parserCallback).execute(resultStream);

	}

}

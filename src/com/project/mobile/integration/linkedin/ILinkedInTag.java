package com.project.mobile.integration.linkedin;

/*
 * LinkedIn Tag Library
 */
public interface ILinkedInTag {

	/**
	 * General Fields
	 */
	public static final String UPDATE = "update";
	public static final String UPDATES = "updates";
	public static final String VALUES = "values";
	public static final String UPDATE_TYPE = "updateType";
	public static final String PICTURE_URL = "pictureUrl";
	public static final String HEADLINE = "headline";
	public static final String CURRENT_STATUS = "currentStatus";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String UPDATE_KEY = "updateKey";
	public static final String TIMESTAMP = "timestamp";
	public static final String UPDATE_CONTENT = "updateContent";
	public static final String PERSON = "person";
	public static final String API_STANDARD_PROFILE_REQUEST = "apiStandardProfileRequest";
	public static final String SITE_STANDARD_PROFILE_REQUEST = "siteStandardProfileRequest";
	public static final String URL = "url";
	public static final String CONNECTIONS = "connections";
	public static final String ID = "id";

	/**
	 * Network Update Types
	 */
	// CONN
	public static final String UPDATE_CONN = "CONN";

	// CMPY
	public static final String UPDATE_CMPY = "CMPY";
	public static final String CMPY_COMPANY_STATUS_UPDATE = "companyStatusUpdate";
	public static final String CMPY_PERSON_UPDATE = "companyPersonUpdate";
	public static final String CMPY_STATUS_UPDATE = "companyStatusUpdate";
	public static final String CMPY_JOB_UPDATE = "companyJobUpdate";
	public static final String COMPANY = "company";
	public static final String JOB = "job";
	public static final String POSITION = "position";
	public static final String TITLE = "title";
	public static final String NAME = "name";
	public static final String SHARE = "share";
	public static final String COMMENT = "comment";
	public static final String CONTENT = "content";
	public static final String THUMBNAIL_URL = "thumbnailUrl";
	public static final String SUBMITTED_URL = "submittedUrl";
	public static final String SITE_JOB_REQUEST = "siteJobRequest";

}

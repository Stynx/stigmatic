package com.project.mobile.integration.linkedin;

import com.project.mobile.communication.GetTask;
import com.project.mobile.communication.IGetCallback;
import com.project.mobile.communication.IRestTaskCallback;
import com.project.mobile.communication.model.HttpResponseModel;
import com.project.mobile.utility.Logger;

/**
 * 
 * LinkedIn integration API
 * 
 */
public class LinkedInApi {

	// url for getting first degree network updates
	private final static String URL_FIRST_DEGREE_UPDATES = "https://api.linkedin.com/v1/people/~/network/updates";

	// url for getting self profile
	private final static String URL_SELF_PROFILE = "https://api.linkedin.com/v1/people/~";

	// url to invalidate access token
	private final static String URL_INVALIDATE_TOKEN = "https://api.linkedin.com/uas/oauth/invalidateToken";

	private String dateFormat;

	// singleton instance
	private static final LinkedInApi INSTANCE = new LinkedInApi();

	private LinkedInApi() {

	}

	public static LinkedInApi getInstance() {

		return INSTANCE;

	}

	/**
	 * Request a first degree updates of the current user from the REST server
	 * 
	 * @param callback
	 *            Callback to execute when the first degree network updates are available
	 * @param start
	 *            start offset
	 * @param count
	 *            paging limit
	 */
	public void getFirstDegreeNetworkUpdates(String accessToken, Integer start, Integer count, Long lastTimestamp, boolean json,
			final IGetCallback getCallback) {

		StringBuffer restUrlBuffer = new StringBuffer(getRestUrlWithAccessToken(accessToken, URL_FIRST_DEGREE_UPDATES));

		if (start != null) {

			restUrlBuffer.append(Config.AMPERSAND);
			restUrlBuffer.append(Config.START);
			restUrlBuffer.append(Config.EQUALS);
			restUrlBuffer.append(start);

		}

		if (count != null) {

			restUrlBuffer.append(Config.AMPERSAND);
			restUrlBuffer.append(Config.COUNT);
			restUrlBuffer.append(Config.EQUALS);
			restUrlBuffer.append(count);

		}

		if (lastTimestamp != null) {

			restUrlBuffer.append(Config.AMPERSAND);
			restUrlBuffer.append(Config.AFTER);
			restUrlBuffer.append(Config.EQUALS);
			restUrlBuffer.append(lastTimestamp);

		}

		if (json) {

			restUrlBuffer.append(Config.AMPERSAND);
			restUrlBuffer.append(Config.FORMAT);
			restUrlBuffer.append(Config.EQUALS);
			restUrlBuffer.append(Config.JSON);

		}

		Logger.log(Logger.TYPE_INFO, restUrlBuffer.toString(), getClass().getName());

		new GetTask(restUrlBuffer.toString(), new IRestTaskCallback() {

			@Override
			public void onTaskComplete(HttpResponseModel responseModel) {

				getCallback.onDataReceived(responseModel);

			}

		}).execute();

	}

	/**
	 * Request to invalidate current access token for logout
	 */
	public void logout(String accessToken, final IGetCallback getCallback) {

		new GetTask(getRestUrlWithAccessToken(accessToken, URL_INVALIDATE_TOKEN).toString(), new IRestTaskCallback() {

			@Override
			public void onTaskComplete(HttpResponseModel responseModel) {

				getCallback.onDataReceived(responseModel);

			}

		}).execute();

	}

	/**
	 * Request a first degree updates of the current user from the REST server
	 * 
	 * @param callback
	 *            Callback to execute when the first degree network updates are available
	 */
	public void getSelfProfile(String accessToken, final IGetCallback getCallback) {

		new GetTask(getRestUrlWithAccessToken(accessToken, URL_SELF_PROFILE), new IRestTaskCallback() {

			@Override
			public void onTaskComplete(HttpResponseModel responseModel) {

				getCallback.onDataReceived(responseModel);

			}

		}).execute();

	}

	/**
	 * Concatanes rest url with users unique access token
	 * 
	 * @param accessToken
	 * @param url
	 * @return
	 */
	public String getRestUrlWithAccessToken(String accessToken, String url) {

		return url + Config.QUESTION_MARK + Config.OAUTH_2_ACCESS_TOKEN + Config.EQUALS + accessToken;

	}

	/**
	 * Method that generates the url for get the access token from the Service
	 * 
	 * @return Url
	 */
	public String getAccessTokenUrl(String authorizationToken) {

		String URL = Config.ACCESS_TOKEN_URL + Config.QUESTION_MARK + Config.GRANT_TYPE_PARAM + Config.EQUALS + Config.GRANT_TYPE
				+ Config.AMPERSAND + Config.RESPONSE_TYPE_VALUE + Config.EQUALS + authorizationToken + Config.AMPERSAND
				+ Config.CLIENT_ID_PARAM + Config.EQUALS + Config.API_KEY + Config.AMPERSAND + Config.REDIRECT_URI_PARAM + Config.EQUALS
				+ Config.REDIRECT_URI + Config.AMPERSAND + Config.SECRET_KEY_PARAM + Config.EQUALS + Config.SECRET_KEY;
		return URL;

	}

	/**
	 * Method that generates the url for get the authorization token from the Service
	 * 
	 * @return Url
	 */
	public String getAuthorizationUrl() {

		String URL = Config.AUTHORIZATION_URL + Config.QUESTION_MARK + Config.RESPONSE_TYPE_PARAM + Config.EQUALS
				+ Config.RESPONSE_TYPE_VALUE + Config.AMPERSAND + Config.CLIENT_ID_PARAM + Config.EQUALS + Config.API_KEY
				+ Config.AMPERSAND + Config.SCOPE_PARAM + Config.EQUALS + Config.SCOPES + Config.AMPERSAND + Config.STATE_PARAM
				+ Config.EQUALS + Config.STATE + Config.AMPERSAND + Config.REDIRECT_URI_PARAM + Config.EQUALS + Config.REDIRECT_URI;

		return URL;

	}

	public static final class Config {

		/**** YOUR LINKEDIN APP INFO HERE *********/

		public static final String API_KEY = "77ghjkif2pqtkb";
		public static final String SECRET_KEY = "QAg94TiidviE9Zi5";

		/**
		 * This is any string we want to use. This will be used for avoid CSRF attacks. You can generate one here:
		 * http://strongpasswordgenerator.com/
		 */
		public static final String STATE = "E3ZYKC1T6H2yP4z";
		public static final String REDIRECT_URI = "http://www.stigmatic.com";

		public static final String SCOPES = "r_fullprofile%20rw_nus%20r_network";

		/*********************************************/

		/**
		 * These are constants used for build the urls
		 */
		public static final String AUTHORIZATION_URL = "https://www.linkedin.com/uas/oauth2/authorization";
		public static final String ACCESS_TOKEN_URL = "https://www.linkedin.com/uas/oauth2/accessToken";
		public static final String SECRET_KEY_PARAM = "client_secret";
		public static final String RESPONSE_TYPE_PARAM = "response_type";
		public static final String GRANT_TYPE_PARAM = "grant_type";
		public static final String GRANT_TYPE = "authorization_code";
		public static final String RESPONSE_TYPE_VALUE = "code";
		public static final String CLIENT_ID_PARAM = "client_id";
		public static final String SCOPE_PARAM = "scope";
		public static final String STATE_PARAM = "state";
		public static final String REDIRECT_URI_PARAM = "redirect_uri";
		public static final String OAUTH_2_ACCESS_TOKEN = "oauth2_access_token";
		public static final String COUNT = "count";
		public static final String START = "start";
		public static final String FORMAT = "format";
		public static final String JSON = "json";
		/*********************************************/

		public static final String QUESTION_MARK = "?";
		public static final String AMPERSAND = "&";
		public static final String EQUALS = "=";
		public static final String AFTER = "after";
		public static final String BEFORE = "before";

	}

	public void setDateFormat(String dateFormat) {

		this.dateFormat = dateFormat;

	}

	public String getDateFormat() {

		if (dateFormat == null)
			return "dd.MM.yyyy HH:mm";

		return dateFormat;

	}

}

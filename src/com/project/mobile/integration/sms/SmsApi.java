package com.project.mobile.integration.sms;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.PhoneLookup;
import android.provider.Telephony.Sms;

import com.project.mobile.utility.DateUtils;
import com.project.mobile.utility.PhoneUtils;
import com.project.stigmatic.configuration.ApplicationSettings;
import com.project.stigmatic.model.StigmaticModel;

/**
 * 
 * This class retrieves messages from the phone. "<uses-permission android:name="android.permission.READ_SMS" />" is required in the
 * manifest.
 * 
 */
@SuppressLint("InlinedApi")
public class SmsApi {

	// singleton instance
	private static final SmsApi INSTANCE = new SmsApi();

	// date format
	private String dateFormat;

	// main uri to retrieve conversations
	private final String SMS_URI = "content://mms-sms/conversations/";

	private SmsApi() {

	}

	public static SmsApi getInstance() {

		return INSTANCE;

	}

	/**
	 * Fetches all messages from device. Required permission need to be set in the manifest file.
	 * 
	 * @return
	 */
	public List<StigmaticModel> getMessageList(Context c, Long lastTimestamp) {

		ArrayList<StigmaticModel> messageList = new ArrayList<StigmaticModel>();

		Uri uriSMS = Uri.parse("content://sms/inbox");

		Cursor smsCursor = c.getContentResolver().query(uriSMS, null, "date > ?", new String[] { String.valueOf(lastTimestamp) }, null);

		StigmaticModel smsItem = null;

		int smsID = 0;

		Cursor contactCursor = null;

		ContentResolver resolver = null;

		try {

			while (smsCursor.moveToNext()) {

				smsItem = new StigmaticModel();

				// Get contact display name and id
				resolver = c.getContentResolver();
				Uri lookupUri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(smsCursor.getString(2).replace("+", "")));
				contactCursor = resolver.query(lookupUri, new String[] { PhoneLookup.DISPLAY_NAME, PhoneLookup._ID }, null, null, null);
				try {

					contactCursor.moveToFirst();

				} catch (Exception e) {

				}

				String contactName = smsCursor.getString(2);

				smsID = smsCursor.getInt(smsCursor.getColumnIndex(Sms.THREAD_ID));

				try {

					contactName = contactCursor.getString(0);

				} catch (Exception e) {

					// Logger.log(Logger.TYPE_ERROR, e.getMessage(), getClass().getName());

				}

				try {

					int contactID = Integer.parseInt(contactCursor.getString(1));

					// Get contact photo
					if (contactID != 0) {

						smsItem.setPhoto(PhoneUtils.openContactPhoto(contactID, c));

					}

				} catch (Exception e) {

					smsItem.setPhoto(null);

					// Logger.log(Logger.TYPE_ERROR, e.getMessage(), getClass().getName());

				}

				String dateFormat = this.dateFormat != null ? this.dateFormat : "dd.MM.yyyy HH:mm";

				long timestamp = Long.parseLong(smsCursor.getString(5));

				String dateAsString = DateUtils.getDateStringFromTimestamp(dateFormat, timestamp);

				// Set timestamp
				smsItem.setTimestamp(timestamp);
				// Set sms date
				smsItem.setDate(dateAsString);
				// Set contact name
				smsItem.setUser(contactName);
				// Set sms body
				smsItem.setMessage(smsCursor.getString(12));
				// Set platform
				smsItem.setPlatform(ApplicationSettings.PLATFORM_SMS);

				smsItem.setLink(SMS_URI.concat(Integer.toString(smsID)));

				messageList.add(smsItem);

				if (contactCursor != null)
					contactCursor.close();

			}

		} finally {

			if (smsCursor != null)
				smsCursor.close();

		}

		return messageList;

	}

	public void setDateFormat(String dateFormat) {

		this.dateFormat = dateFormat;

	}

}

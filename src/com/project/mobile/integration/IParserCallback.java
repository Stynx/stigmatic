package com.project.mobile.integration;

import java.util.List;

/**
 * 
 * Callback for parser results.
 * 
 */
public interface IParserCallback<T> {

	/**
	 * Called when a parser result list is received.
	 */
	void onParserDataReceived(List<T> resultList);

}

package com.project.mobile.integration.facebook;

/**
 * 
 * Facebook general field library for api
 * 
 */
public interface IFacebookTag {

	public static final String DATA = "data";
	public static final String TYPE = "type";
	public static final String NAME = "name";
	public static final String STATUS_TYPE = "status_type";
	public static final String TO = "to";
	public static final String FROM = "from";
	public static final String MESSAGE = "message";
	public static final String STORY = "story";
	public static final String ID = "id";
	public static final String APPLICATION = "application";
	public static final String INSTAGRAM = "Instagram";
	public static final String LIKES_PHOTOS = "Likes photos on";
	public static final String CREATED_TIME = "created_time";
	public static final String SLASH = "/";
	
	public static final String WALL_POST = "wall_post";

	public static final String LINK = "link";
	public static final String PICTURE = "picture";

	public static final String DATE_FORMAT = "yyyyy-MM-dd HH:mm:ssZ";

	public static final String FACEBOOK_URL = "http://www.facebook.com/";
	public static final String FACEBOOK_APP_FEED_URL = "fb://post/";
	public static final String FACEBOOK_APP_USER_URL = "fb://user/";
	public static final String FACEBOOK_GRAPH_URL = "http://graph.facebook.com/";

}

package com.project.mobile.integration.facebook;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

import com.project.mobile.integration.IParserCallback;
import com.project.mobile.integration.facebook.model.FacebookModel;
import com.project.mobile.utility.ConvertUtils;
import com.project.mobile.utility.DateUtils;
import com.project.mobile.utility.Logger;
import com.project.stigmatic.configuration.ApplicationSettings;

/**
 * 
 * Facebook parser. It reads input stream that is sent as json.
 * 
 */
public class FacebookParser {

	// singleton instance
	private static final FacebookParser INSTANCE = new FacebookParser();

	private FacebookParser() {

	}

	public static FacebookParser getInstance() {

		return INSTANCE;

	}

	private final class FacebookTimelineParser extends AsyncTask<InputStream, Integer, List<FacebookModel>> {

		private IParserCallback<FacebookModel> parserCallback;

		public FacebookTimelineParser(IParserCallback<FacebookModel> parserCallback) {

			this.parserCallback = parserCallback;

		}

		@Override
		protected List<FacebookModel> doInBackground(InputStream... params) {

			InputStream resultStream = params[0];
			String jsonString = ConvertUtils.getStringFromInputStream(resultStream);

			List<FacebookModel> list = new ArrayList<FacebookModel>();

			JSONObject jsonResponse;

			try {

				jsonResponse = new JSONObject(jsonString);
				JSONArray feedNodes = jsonResponse.getJSONArray(IFacebookTag.DATA);
				int feedLenght = feedNodes.length();

				JSONObject iteratorNode = null, previousNode;
				JSONArray iteratorArray = null;

				String feedLink = "";
				String feedDate = "";

				long timestamp = 0;
				FacebookModel facebookModel = null;
				
				String toUser = "";
				String statusType = "";

				for (int i = 0; i < feedLenght; i++) {

					facebookModel = new FacebookModel();

					iteratorNode = feedNodes.getJSONObject(i);
					previousNode = feedNodes.getJSONObject(i);

					// facebook item type
					if (iteratorNode.has(IFacebookTag.TYPE)) {

						facebookModel.setType(iteratorNode.getString(IFacebookTag.TYPE));

					}

					// status type
					if (iteratorNode.has(IFacebookTag.STATUS_TYPE)) {

						facebookModel.setStatusType(iteratorNode.getString(IFacebookTag.STATUS_TYPE));

					}

					// feed id and url
					if (iteratorNode.has(IFacebookTag.ID)) {

						facebookModel.setFeedID(iteratorNode.get(IFacebookTag.ID).toString());
						facebookModel.setFacebookLink(IFacebookTag.FACEBOOK_APP_FEED_URL.concat(facebookModel.getFeedID()));

					}

					// username and user id that shares the item
					if (iteratorNode.has(IFacebookTag.FROM)) {

						iteratorNode = iteratorNode.getJSONObject(IFacebookTag.FROM);

						if (iteratorNode.has(IFacebookTag.NAME)) {

							facebookModel.setUserName(iteratorNode.getString(IFacebookTag.NAME));

						}

						if (iteratorNode.has(IFacebookTag.ID)) {

							facebookModel.setUserID(iteratorNode.getString(IFacebookTag.ID));

						} else {

							facebookModel.setUserID("");

						}

						if (facebookModel.getFacebookLink() == "") {

							facebookModel.setFacebookLink(IFacebookTag.FACEBOOK_APP_FEED_URL.concat(facebookModel.getUserID()));

						}

					}
					
					if (facebookModel.getUserID() != "") {

						facebookModel.setUserPhotoUrl(IFacebookTag.FACEBOOK_GRAPH_URL.concat(facebookModel.getUserID())
								.concat(IFacebookTag.SLASH).concat(IFacebookTag.PICTURE));

					}
					
					iteratorNode = previousNode;
					
					if (iteratorNode.has(IFacebookTag.STATUS_TYPE)) {
						
						statusType = iteratorNode.getString(IFacebookTag.STATUS_TYPE);
						
					}
					
					if (iteratorNode.has(IFacebookTag.TO) && statusType.equals(IFacebookTag.WALL_POST)){
						
						iteratorNode = iteratorNode.getJSONObject(IFacebookTag.TO);
						
						iteratorArray = iteratorNode.getJSONArray(IFacebookTag.DATA);
						
						iteratorNode = iteratorArray.getJSONObject(0);
						
						if (iteratorNode.has(IFacebookTag.NAME)) {

							toUser = (iteratorNode.getString(IFacebookTag.NAME));
							
							facebookModel.setUserName(facebookModel.getUserName().concat(" > ").concat(toUser));

						}
						
					}
					
					iteratorNode = previousNode;

					if (iteratorNode.has(IFacebookTag.MESSAGE)) {

						facebookModel.setMessage(iteratorNode.getString(IFacebookTag.MESSAGE));

					} else if (iteratorNode.has(IFacebookTag.STORY)) {

						facebookModel.setMessage(iteratorNode.getString(IFacebookTag.STORY));

					} else if (iteratorNode.has(IFacebookTag.NAME)) {

						facebookModel.setMessage(iteratorNode.getString(IFacebookTag.NAME));

					} else if (iteratorNode.has(IFacebookTag.APPLICATION)) {

						iteratorNode = iteratorNode.getJSONObject(IFacebookTag.APPLICATION);

						if (iteratorNode.has(IFacebookTag.NAME)) {

							if (iteratorNode.getString(IFacebookTag.NAME).equals(IFacebookTag.INSTAGRAM)) {

								facebookModel.setMessage(IFacebookTag.LIKES_PHOTOS.concat(" ").concat(IFacebookTag.INSTAGRAM));

							}

						}

						iteratorNode = previousNode;

					}

					if (iteratorNode.has(IFacebookTag.CREATED_TIME)) {

						feedDate = iteratorNode.getString(IFacebookTag.CREATED_TIME);
						feedDate = feedDate.replace("T", " ");
						// Faceobook Date format
						SimpleDateFormat dt = new SimpleDateFormat(IFacebookTag.DATE_FORMAT);
						// Parse feed date
						Date date = dt.parse(feedDate);
						Calendar cal = Calendar.getInstance();
						cal.setTime(date);
						// Long timestamp = date.getTime();
						timestamp = cal.getTimeInMillis();
						facebookModel.setTimestamp(timestamp);
						facebookModel.setCreatedTime(DateUtils.getDateStringFromTimestamp(ApplicationSettings.DATE_FORMAT, timestamp));

					}

					if (iteratorNode.has(IFacebookTag.LINK)) {

						feedLink = iteratorNode.getString(IFacebookTag.LINK);
						facebookModel.setLink(feedLink);

					} else {

						if (facebookModel.getUserID() != "") {

							facebookModel.setLink(IFacebookTag.FACEBOOK_URL.concat(facebookModel.getUserID()));

						}

					}

					if (iteratorNode.has(IFacebookTag.PICTURE)) {

						facebookModel.setContentPhotoUrl(iteratorNode.getString(IFacebookTag.PICTURE));

					}

					list.add(facebookModel);
				}

			} catch (JSONException e) {

				Logger.log(Logger.TYPE_ERROR, e.getMessage(), getClass().getName());

			} catch (ParseException e) {

				Logger.log(Logger.TYPE_ERROR, e.getMessage(), getClass().getName());

			}

			return list;

		}

		@Override
		protected void onPostExecute(List<FacebookModel> resultList) {

			parserCallback.onParserDataReceived(resultList);
			super.onPostExecute(resultList);

		}

	}

	public void getFacebookTimelineUpdateList(InputStream resultStream, IParserCallback<FacebookModel> parserCallback) {

		new FacebookTimelineParser(parserCallback).execute(resultStream);

	}

}
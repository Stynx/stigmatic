package com.project.mobile.integration.facebook;

import com.project.mobile.communication.GetTask;
import com.project.mobile.communication.IGetCallback;
import com.project.mobile.communication.IRestTaskCallback;
import com.project.mobile.communication.model.HttpResponseModel;
import com.project.mobile.utility.Logger;

/**
 * 
 * Facebook integration API
 * 
 */
public class FacebookApi {

	// url for getting self profile
	private final static String URL_SELF_HOME = "https://graph.facebook.com/me/home";

	// singleton instance
	private static final FacebookApi INSTANCE = new FacebookApi();

	private static boolean firstCall = true;

	private FacebookApi() {

	}

	public static FacebookApi getInstance() {

		return INSTANCE;

	}

	/**
	 * Downloads user newsfeed
	 * 
	 * @param accessToken
	 * @param lastTimeStamp
	 * @param hasTimestamp
	 * @param getCallback
	 */
	public void getSelfProfile(String accessToken, long lastTimeStamp, boolean hasTimestamp, final IGetCallback getCallback) {

		String restUrl = getRestUrlWithAccessToken(accessToken, URL_SELF_HOME, lastTimeStamp, hasTimestamp);

		Logger.log(Logger.TYPE_INFO, restUrl, getClass().getName());

		new GetTask(restUrl, new IRestTaskCallback() {

			@Override
			public void onTaskComplete(HttpResponseModel responseModel) {

				getCallback.onDataReceived(responseModel);

			}

		}).execute();

	}

	/**
	 * Prepares rest url with access token.
	 * 
	 * @param accessToken
	 *            Access token is used to get authorized.
	 * @param url
	 *            Rest Url
	 * @param timestamp
	 *            Timestamp is used to fetch new items after specified date. Therefore less data will be used.
	 * @param withTimestamp
	 *            If withTimestamp is set as true, last time stamp is used otherwise not.
	 * @return
	 */
	private static String getRestUrlWithAccessToken(String accessToken, String url, long timestamp, boolean withTimestamp) {

		if (withTimestamp) {

			int limit = firstCall ? 75 : Config.LIMIT;

			if (firstCall)
				firstCall = false;

			return url + Config.QUESTION_MARK + Config.OAUTH_2_ACCESS_TOKEN + Config.EQUALS + accessToken + Config.AMPERSAND + Config.SINCE
					+ Config.EQUALS + timestamp + Config.AMPERSAND + Config.LIMIT_PARAM + Config.EQUALS + limit;

		} else {

			return url + Config.QUESTION_MARK + Config.OAUTH_2_ACCESS_TOKEN + Config.EQUALS + accessToken + Config.AMPERSAND
					+ Config.LIMIT_PARAM + Config.EQUALS + Config.LIMIT;

		}

	}

	/**
	 * Prepares url that gets access token for Facebook
	 * 
	 * @param authorizationToken
	 * @return
	 */
	public static String getAccessTokenUrl(String authorizationToken) {

		String URL = Config.ACCESS_TOKEN_URL + Config.QUESTION_MARK + Config.CLIENT_ID_PARAM + Config.EQUALS + Config.API_KEY
				+ Config.AMPERSAND + Config.REDIRECT_URI_PARAM + Config.EQUALS + Config.REDIRECT_URI + Config.AMPERSAND
				+ Config.SECRET_KEY_PARAM + Config.EQUALS + Config.SECRET_KEY + Config.AMPERSAND + Config.RESPONSE_TYPE_VALUE
				+ Config.EQUALS + authorizationToken;
		return URL;

	}

	/**
	 * Method that generates the url to get the authorization token from the Service
	 * 
	 * @return Url
	 */
	public static String getAuthorizationUrl() {

		String URL = Config.AUTHORIZATION_URL + Config.QUESTION_MARK + Config.CLIENT_ID_PARAM + Config.EQUALS + Config.API_KEY
				+ Config.AMPERSAND + Config.REDIRECT_URI_PARAM + Config.EQUALS + Config.REDIRECT_URI + Config.AMPERSAND
				+ Config.SCOPE_PARAM + Config.EQUALS + Config.SCOPES + Config.AMPERSAND + Config.STATE_PARAM + Config.EQUALS + Config.STATE
				+ Config.AMPERSAND + Config.RESPONSE_TYPE_PARAM + Config.EQUALS + Config.RESPONSE_TYPE_VALUE;

		return URL;

	}

	public static final class Config {

		/* CONSTANT FOR THE AUTHORIZATION PROCESS */

		/**** YOUR FACEBOOK APP INFO HERE *********/

		public static final String API_KEY = "1485903511625768";
		public static final String SECRET_KEY = "ffcff4d19a67dcd59af52d136dc29078";

		/**
		 * This is any string we want to use. This will be used for avoid CSRF attacks. You can generate one here:
		 * http://strongpasswordgenerator.com/
		 */
		public static final String STATE = "E3ZYKC1T6H2yP4z";
		public static final String REDIRECT_URI = "http://www.stigmatic.com/";
		public static final String SCOPES = "read_stream,user_checkins,friends_checkins,read_mailbox,user_actions:instapp,friends_actions:instapp";

		// https://m.facebook.com/login.php?skip_api_login=1&api_key=1485903511625768&
		// signed_next=1&next=https%3A%2F%2Fm.facebook.com%2Fdialog%2Foauth%3F
		// redirect_uri%3Dhttp%253A%252F%252Fwww.stigmatic.com%26client_id%3D1485903511625768%26ret%3Dlogin&cancel_uri=http%3A%2F%2Fwww.stigmatic.com%2F%3Ferror%3Daccess_denied%26error_code%3D200%26error_description%3DPermissions%2Berror%26error_reason%3Duser_denied%23_%3D_&display=touch&_rdr

		/*********************************************/

		/**
		 * these are constants used for build the urls
		 */
		public static final String AUTHORIZATION_URL = "https://m.facebook.com/dialog/oauth";
		public static final String ACCESS_TOKEN_URL = "https://graph.facebook.com/oauth/access_token";
		public static final String CHECKPOINT_URL = "https://m.facebook.com/checkpoint/?next=https%3A%2F%2Fm.facebook.com%2Fdialog%2Foauth";
		public static final String LOGIN_URL = "https://m.facebook.com/login.php";
		public static final String SECRET_KEY_PARAM = "client_secret";
		public static final String RESPONSE_TYPE_PARAM = "response_type";
		public static final String GRANT_TYPE_PARAM = "grant_type";
		public static final String GRANT_TYPE = "authorization_code";
		public static final String RESPONSE_TYPE_VALUE = "code";
		public static final String CLIENT_ID_PARAM = "client_id";
		public static final String SCOPE_PARAM = "scope";
		public static final String STATE_PARAM = "state";
		public static final String REDIRECT_URI_PARAM = "redirect_uri";
		public static final String OAUTH_2_ACCESS_TOKEN = "access_token";
		public static final String LIMIT_PARAM = "limit";
		public static final int LIMIT = 1000;
		public static final String SINCE = "since";

		/*********************************************/

		public static final String QUESTION_MARK = "?";
		public static final String AMPERSAND = "&";
		public static final String EQUALS = "=";

	}

}

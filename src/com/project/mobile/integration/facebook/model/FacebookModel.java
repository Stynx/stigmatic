package com.project.mobile.integration.facebook.model;

/**
 * 
 * Facebook model for api
 * 
 */
public class FacebookModel {

	private String feedID;
	private String userName;
	private String userID;
	private String message;

	private String privacy;
	private String type;
	private String statusType;
	private String createdTime;
	private String updatedTime;
	private String sharesCount;

	private String link;
	private String facebookLink;
	private String userPhotoUrl;
	private String contentPhotoUrl;
	private long timestamp;

	public String getFeedID() {

		return feedID;

	}

	public void setFeedID(String feedID) {

		this.feedID = feedID;

	}

	public String getUserName() {

		return userName;

	}

	public void setUserName(String fromUserName) {

		this.userName = fromUserName;

	}

	public String getUserID() {

		return userID;

	}

	public void setUserID(String fromUserID) {

		this.userID = fromUserID;

	}

	public String getMessage() {

		return message;

	}

	public void setMessage(String message) {

		this.message = message;

	}

	public String getPrivacy() {

		return privacy;

	}

	public void setPrivacy(String privacy) {

		this.privacy = privacy;

	}

	public String getType() {

		return type;

	}

	public void setType(String type) {

		this.type = type;

	}

	public String getStatusType() {

		return statusType;

	}

	public void setStatusType(String statusType) {

		this.statusType = statusType;

	}

	public String getCreatedTime() {

		return createdTime;

	}

	public void setCreatedTime(String createdTime) {

		this.createdTime = createdTime;

	}

	public String getUpdatedTime() {

		return updatedTime;

	}

	public void setUpdatedTime(String updatedTime) {

		this.updatedTime = updatedTime;

	}

	public String getSharesCount() {

		return sharesCount;

	}

	public void setSharesCount(String sharesCount) {

		this.sharesCount = sharesCount;

	}

	public String getLink() {

		return link;

	}

	public void setLink(String link) {

		this.link = link;

	}

	public String getFacebookLink() {

		return facebookLink;

	}

	public void setFacebookLink(String facebookLink) {

		this.facebookLink = facebookLink;

	}

	public String getUserPhotoUrl() {

		return userPhotoUrl;

	}

	public void setUserPhotoUrl(String userPhotoUrl) {

		this.userPhotoUrl = userPhotoUrl;

	}

	public String getContentPhotoUrl() {

		return contentPhotoUrl;

	}

	public void setContentPhotoUrl(String contentPhotoUrl) {

		this.contentPhotoUrl = contentPhotoUrl;

	}

	public long getTimestamp() {

		return timestamp;

	}

	public void setTimestamp(long timestamp) {

		this.timestamp = timestamp;

	}

}

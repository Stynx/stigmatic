package com.project.stigmatic.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.project.stigmatic.configuration.ApplicationSettings;
import com.project.stigmatic.fragment.AboutFragment;
import com.project.stigmatic.fragment.SettingsFragment;

/**
 * Adapter class that provides tab fragments with views
 * 
 */
public class TabPagerAdapter extends FragmentPagerAdapter {

	private AboutFragment aboutFragment;
	private SettingsFragment settingsFragment;

	public TabPagerAdapter(FragmentManager fragmentManager) {

		super(fragmentManager);

	}

	/**
	 * Creates and returns corresponding fragment when user swipes or clicks a tab on the action bar
	 */
	@Override
	public Fragment getItem(int index) {

		switch (index) {

		case ApplicationSettings.INDEX_FRAGMENT_SETTINGS:

			if (settingsFragment == null)
				settingsFragment = new SettingsFragment();

			return settingsFragment;

		case ApplicationSettings.INDEX_FRAGMENT_ABOUT:

			if (aboutFragment == null)
				aboutFragment = new AboutFragment();

			return aboutFragment;

		}

		return null;

	}

	@Override
	public int getCount() {

		// get item count - equal to number of tabs in the application
		return 2;

	}

}

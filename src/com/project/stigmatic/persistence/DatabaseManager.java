package com.project.stigmatic.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import com.project.stigmatic.configuration.ApplicationSettings;
import com.project.stigmatic.configuration.DatabaseSettings;
import com.project.stigmatic.model.StigmaticModel;

/**
 * 
 * Persistence class for stigmatic project consists of main CRUD operations(Create, Read, Update, Destroy) and connection settings
 * 
 */
public class DatabaseManager {

	private SQLiteDatabase database; // database object
	private static DatabaseOpenHelper databaseOpenHelper; // database helper

	private AtomicInteger openCounter = new AtomicInteger();
	private static DatabaseManager instance;

	public static synchronized DatabaseManager getInstance(Context context) {

		if (instance == null) {

			instance = new DatabaseManager();
			databaseOpenHelper = new DatabaseOpenHelper(context, DatabaseSettings.DATABASE_NAME, null, 1);

		}

		return instance;

	}

	public synchronized SQLiteDatabase openDatabase() {

		if (openCounter.incrementAndGet() == 1) {
			// Opening new database

			database = databaseOpenHelper.getWritableDatabase();

		}

		return database;

	}

	public synchronized void closeDatabase() {

		if (openCounter.decrementAndGet() == 0) {

			// Closing database
			database.close();

		}

	}

	/**
	 * Inserts a new stigmatic list item into the database while clearing older items
	 * 
	 * @param model
	 */
	public void insertModelListAfterDeletingOutOfSync(List<StigmaticModel> newModelList) {

		try {

			openDatabase();
			database.beginTransaction();
			Long lowestTimestamp = System.currentTimeMillis() - ApplicationSettings.TIME_ONE_DAY;
			database.delete(DatabaseSettings.TABLE_ITEM,
					DatabaseSettings.COLUMN_TIMESTAMP.concat("<").concat(String.valueOf(lowestTimestamp)), null);
			for (StigmaticModel newModel : newModelList)
				newModel.setPid((int) database.insert(DatabaseSettings.TABLE_ITEM, null, getContentValues(newModel)));
			database.setTransactionSuccessful();

		} catch (Exception ex) {
		} finally {

			database.endTransaction();
			closeDatabase();

		}

	}

	/**
	 * Deletes items which have timestamp lower than sync period
	 * 
	 * @param lowestTimestamp
	 *            Current Time - Sync Time
	 */
	public void deleteOlderItems(long lowestTimestamp) {

		openDatabase();
		database.delete(DatabaseSettings.TABLE_ITEM, DatabaseSettings.COLUMN_TIMESTAMP.concat("<").concat(String.valueOf(lowestTimestamp)),
				null);
		closeDatabase();

	}

	/**
	 * Updates an existing stigmatic list item in the database.
	 * 
	 * @param model
	 */
	public void updateModel(StigmaticModel editModel) {

		openDatabase(); // open the database
		database.update(DatabaseSettings.TABLE_ITEM, getContentValues(editModel),
				DatabaseSettings.COLUMN_PRIMARY_KEY + "=" + editModel.getPid(), null);
		closeDatabase(); // close the database

	}

	/**
	 * Deletes the entire table
	 */
	public void deleteEntireTable() {

		try {

			openDatabase(); // open the database
			database.delete(DatabaseSettings.TABLE_ITEM, null, null);

		} catch (Exception ex) {
		} finally {

			closeDatabase(); // close the database

		}

	}

	/**
	 * Deletes selected platform items
	 * 
	 * @param platformName
	 *            Like Facebook or any other platform
	 */
	public void deleteSelectedPlatformItems(String platformName) {

		openDatabase(); // open the database
		database.delete(DatabaseSettings.TABLE_ITEM, DatabaseSettings.COLUMN_PLATFORM.concat("=").concat("?"),
				new String[] { platformName });
		closeDatabase(); // close the database

	}

	/**
	 * Prepares container for sqlite functions
	 * 
	 * @param model
	 * @return
	 */
	private ContentValues getContentValues(StigmaticModel model) {

		ContentValues contentValues = new ContentValues();
		contentValues.put(DatabaseSettings.COLUMN_USER, model.getUser());
		contentValues.put(DatabaseSettings.COLUMN_HEADER, model.getHeader());
		contentValues.put(DatabaseSettings.COLUMN_MESSAGE, model.getMessage());
		contentValues.put(DatabaseSettings.COLUMN_PLATFORM, model.getPlatform());
		contentValues.put(DatabaseSettings.COLUMN_LINK, model.getLink());
		contentValues.put(DatabaseSettings.COLUMN_NATIVE_APP_LINK, model.getNativeAppLink());
		contentValues.put(DatabaseSettings.COLUMN_DATE, model.getDate());
		contentValues.put(DatabaseSettings.COLUMN_TIMESTAMP, String.valueOf(model.getTimestamp()));
		contentValues.put(DatabaseSettings.COLUMN_PHOTO, model.getPhoto());
		contentValues.put(DatabaseSettings.COLUMN_PHOTO_URL, model.getPhotoUrl());
		contentValues.put(DatabaseSettings.COLUMN_CONTENT_PHOTO, model.getContentPhoto());
		contentValues.put(DatabaseSettings.COLUMN_CONTENT_PHOTO_URL, model.getContentPhotoUrl());

		return contentValues;

	}

	/**
	 * Returns a Cursor with all stigmatic list items in the database in selected platform and ordered by timestamp. Last timestamp value is
	 * used for efficient paging.
	 * 
	 * @param platform
	 * @param lastTimestamp
	 * @param paging
	 * @return
	 */
	public List<StigmaticModel> getModelListByCriteria(Long lastTimestamp, boolean paging) {

		openDatabase();

		String selection = lastTimestamp != null ? DatabaseSettings.COLUMN_TIMESTAMP + "> ?" : null;

		String[] arguments = lastTimestamp != null ? new String[] { String.valueOf(lastTimestamp) } : null;

		Cursor result = null;

		List<StigmaticModel> newItemList = new ArrayList<StigmaticModel>();

		try {

			result = database.query(DatabaseSettings.TABLE_ITEM, null, selection, arguments, null, null, DatabaseSettings.COLUMN_TIMESTAMP
					+ " DESC", paging ? String.valueOf(ApplicationSettings.PAGING_LIMIT) : null);

			StigmaticModel model = null;

			while (result.moveToNext()) {

				model = new StigmaticModel();
				model.setPid(result.getInt(result.getColumnIndex(DatabaseSettings.COLUMN_PRIMARY_KEY)));
				model.setUser(result.getString(result.getColumnIndex(DatabaseSettings.COLUMN_USER)));
				model.setHeader(result.getString(result.getColumnIndex(DatabaseSettings.COLUMN_HEADER)));
				model.setMessage(result.getString(result.getColumnIndex(DatabaseSettings.COLUMN_MESSAGE)));
				model.setPlatform(result.getString(result.getColumnIndex(DatabaseSettings.COLUMN_PLATFORM)));
				model.setLink(result.getString(result.getColumnIndex(DatabaseSettings.COLUMN_LINK)));
				model.setNativeAppLink(result.getString(result.getColumnIndex(DatabaseSettings.COLUMN_NATIVE_APP_LINK)));
				model.setDate(result.getString(result.getColumnIndex(DatabaseSettings.COLUMN_DATE)));
				model.setTimestamp(Long.parseLong(result.getString(result.getColumnIndex(DatabaseSettings.COLUMN_TIMESTAMP))));
				model.setPhoto(result.getBlob(result.getColumnIndex(DatabaseSettings.COLUMN_PHOTO)));
				model.setPhotoUrl(result.getString(result.getColumnIndex(DatabaseSettings.COLUMN_PHOTO_URL)));
				model.setContentPhoto(result.getBlob(result.getColumnIndex(DatabaseSettings.COLUMN_CONTENT_PHOTO)));
				model.setContentPhotoUrl(result.getString(result.getColumnIndex(DatabaseSettings.COLUMN_CONTENT_PHOTO_URL)));

				newItemList.add(model);

			}

		} finally {

			if (result != null)
				result.close();

			closeDatabase();

		}

		return newItemList;

	}

	// inner SqLiteOpenHelper class for creating and maintaining the schema
	private static class DatabaseOpenHelper extends SQLiteOpenHelper {

		public DatabaseOpenHelper(Context context, String name, CursorFactory factory, int version) {

			super(context, name, factory, version);

		}

		// creates the stigmatic item table when the database is created
		@Override
		public void onCreate(SQLiteDatabase db) {

			// query to create a table
			String createQuery = "CREATE TABLE " + DatabaseSettings.TABLE_ITEM + "(" + DatabaseSettings.COLUMN_PRIMARY_KEY
					+ " integer primary key autoincrement," + DatabaseSettings.COLUMN_USER + " TEXT," + DatabaseSettings.COLUMN_HEADER
					+ " TEXT," + DatabaseSettings.COLUMN_MESSAGE + " TEXT," + DatabaseSettings.COLUMN_PLATFORM + " TEXT,"
					+ DatabaseSettings.COLUMN_LINK + " TEXT," + DatabaseSettings.COLUMN_NATIVE_APP_LINK + " TEXT,"
					+ DatabaseSettings.COLUMN_DATE + " TEXT," + DatabaseSettings.COLUMN_TIMESTAMP + " TEXT,"
					+ DatabaseSettings.COLUMN_PHOTO + " BLOB," + DatabaseSettings.COLUMN_PHOTO_URL + " TEXT,"
					+ DatabaseSettings.COLUMN_CONTENT_PHOTO + " BLOB," + DatabaseSettings.COLUMN_CONTENT_PHOTO_URL + " TEXT)";

			db.execSQL(createQuery); // execute the query

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		}

	}

}

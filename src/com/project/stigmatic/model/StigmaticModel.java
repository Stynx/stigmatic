package com.project.stigmatic.model;

import java.util.Comparator;

import android.graphics.Bitmap;

/**
 * 
 * Entity for each item which is shown on the widget
 * 
 */
public class StigmaticModel {

	// primary key
	private Integer pid;
	// name or nick
	private String user;
	// head line if exists
	private String header;
	// message or content
	private String message;
	// platform like Facebook,LinkedIn,Sms
	private String platform;
	// link to content
	private String link;
	// link to application content
	private String nativeAppLink;
	// content creation date
	private String date;
	// date as timestamp
	private long timestamp;
	// user profile photo if exists
	private byte[] photo;
	// user profile image url if exists
	private String photoUrl;
	// user profile image in Bitmap format
	private Bitmap photoBitmap;
	// content image if exists
	private byte[] contentPhoto;
	// content image url if exists
	private String contentPhotoUrl;
	// user content image in Bitmap format
	private Bitmap contentPhotoBitmap;
	// transient field which indicates that images persisted before so we are
	// preventing recurring updates..
	private boolean imageSavedBefore;

	public StigmaticModel() {

		timestamp = -1;

	}

	public StigmaticModel(String user, String header, String message, String platform, String link, String date, long timestamp,
			String photoUrl, String contentPhotoUrl) {

		this.user = user;
		this.header = header;
		this.message = message;
		this.platform = platform;
		this.link = link;
		this.date = date;
		this.timestamp = timestamp;
		this.photoUrl = photoUrl;
		this.contentPhotoUrl = contentPhotoUrl;

	}

	public String getUser() {

		if (user == null)
			return "";

		return user;

	}

	public void setUser(String user) {

		this.user = user;

	}

	public String getMessage() {

		if (message == null)
			return "";

		return message;

	}

	public void setMessage(String message) {

		this.message = message;

	}

	public String getPlatform() {

		if (platform == null)
			return "";

		return platform;

	}

	public void setPlatform(String platform) {

		this.platform = platform;

	}

	public String getLink() {

		if (link == null)
			return "";

		return link;

	}

	public void setLink(String link) {

		this.link = link;

	}

	public String getNativeAppLink() {

		if (nativeAppLink == null)
			return "";

		return nativeAppLink;

	}

	public void setNativeAppLink(String nativeAppLink) {

		this.nativeAppLink = nativeAppLink;

	}

	public String getDate() {

		if (date == null)
			return "";

		return date;

	}

	public void setDate(String date) {

		this.date = date;

	}

	public Integer getPid() {

		return pid;

	}

	public void setPid(Integer pid) {

		this.pid = pid;

	}

	public byte[] getPhoto() {

		return photo;

	}

	public void setPhoto(byte[] photo) {

		this.photo = photo;

	}

	public String getHeader() {

		if (header == null)
			return "";

		return header;

	}

	public void setHeader(String header) {

		this.header = header;

	}

	public long getTimestamp() {

		return timestamp;

	}

	public void setTimestamp(long timestamp) {

		this.timestamp = timestamp;

	}

	public String getPhotoUrl() {

		return photoUrl;

	}

	public void setPhotoUrl(String photoUrl) {

		this.photoUrl = photoUrl;

	}

	public byte[] getContentPhoto() {

		return contentPhoto;

	}

	public void setContentPhoto(byte[] contentPhoto) {

		this.contentPhoto = contentPhoto;

	}

	public String getContentPhotoUrl() {

		return contentPhotoUrl;

	}

	public void setContentPhotoUrl(String contentPhotoUrl) {

		this.contentPhotoUrl = contentPhotoUrl;

	}

	public Bitmap getPhotoBitmap() {

		return photoBitmap;

	}

	public void setPhotoBitmap(Bitmap photoBitmap) {

		this.photoBitmap = photoBitmap;

	}

	public Bitmap getContentPhotoBitmap() {

		return contentPhotoBitmap;

	}

	public void setContentPhotoBitmap(Bitmap contentPhotoBitmap) {

		this.contentPhotoBitmap = contentPhotoBitmap;

	}

	public boolean isImageSavedBefore() {

		return imageSavedBefore;

	}

	public void setImageSavedBefore(boolean imageSavedBefore) {

		this.imageSavedBefore = imageSavedBefore;

	}

	public static Comparator<StigmaticModel> TimestampComparator = new Comparator<StigmaticModel>() {

		public int compare(StigmaticModel model1, StigmaticModel model2) {

			// descending order
			return Long.valueOf(model2.getTimestamp()).compareTo(Long.valueOf(model1.getTimestamp()));

		}

	};

}

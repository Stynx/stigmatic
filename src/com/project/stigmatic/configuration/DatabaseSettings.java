package com.project.stigmatic.configuration;

/**
 * Persistence settings including db name and column names etc..
 * 
 */
public interface DatabaseSettings {

	// Database name
	public final static String DATABASE_NAME = "Stigmatic";

	// items table name and corresponding column names
	public final static String TABLE_ITEM = "items";

	// unique key for each item
	public final static String COLUMN_PRIMARY_KEY = "pid";

	// user that sends corresponding message
	public final static String COLUMN_USER = "user";

	// user that sends corresponding message
	public final static String COLUMN_HEADER = "header";

	// platform based message
	public final static String COLUMN_MESSAGE = "message";

	// corresponding platform
	public final static String COLUMN_PLATFORM = "platform";

	// message link to app
	public final static String COLUMN_LINK = "link";
	
	// message link to the native app
	public final static String COLUMN_NATIVE_APP_LINK = "nativeapplink";

	// corresponding date message created at
	public final static String COLUMN_DATE = "date";

	// message creation date as timestap
	public final static String COLUMN_TIMESTAMP = "timestamp";

	// user or company profile photo as byte array
	public final static String COLUMN_PHOTO = "photo";

	// user or company profile photo url
	public final static String COLUMN_PHOTO_URL = "photo_url";

	// content photo as byte array
	public final static String COLUMN_CONTENT_PHOTO = "content_photo";

	// content photo url
	public final static String COLUMN_CONTENT_PHOTO_URL = "content_photo_url";

}

package com.project.stigmatic.configuration;

/**
 * Application settings like paging limits or interface settings
 * 
 */
public interface ApplicationSettings {

	// shared preferences name
	public final static String SHARED_PREFERENCES_NAME = "stigmatic_shared_preferences";

	// about html name
	public final static String ABOUT_HTML_NAME = "about.html";

	// platform keys
	public final static String PLATFORM_LINKEDIN = "platform_linkedin";
	public final static String PLATFORM_FACEBOOK = "platform_facebook";
	public final static String PLATFORM_SMS = "platform_sms";
	public final static String PLATFORM_CALL = "platform_call";

	public final static String KEY_EXPIRES = "expires";
	public final static String KEY_ACCESS_TOKEN = "accessToken";
	public final static String KEY_TIMESTAMP = "timestamp";
	public final static String KEY_SYNC_PERIOD = "syncPeriod";
	public final static String KEY_UPDATE_INTERVAL = "updateInterval";

	// fragment position indexes
	public final static int INDEX_FRAGMENT_SETTINGS = 0;
	public final static int INDEX_FRAGMENT_ABOUT = 1;

	// application specific
	public final static String APPLICATION_COLD_START = "coldStart";

	// paging limit value
	public final static int PAGING_LIMIT = 15;

	// date format
	public final static String DATE_FORMAT = "dd.MM.yyyy HH:mm";

	// time in milli seconds
	public final static long TIME_FIFTEEN_MINUTES = 900000;
	public final static long TIME_THIRTY_MINUTES = 1800000;
	public final static long TIME_ONE_HOUR = 3600000;
	public final static long TIME_TWO_HOURS = 7200000;
	public final static long TIME_THREE_HOURS = 10800000;
	public final static long TIME_SIX_HOURS = 21600000;
	public final static long TIME_TWELVE_HOURS = 43200000;
	public final static long TIME_ONE_DAY = 86400000;

	// time label
	public final static String TIME_LABEL_FIFTEEN_MINUTES = "15 minutes";
	public final static String TIME_LABEL_THIRTY_MINUTES = "30 minutes";
	public final static String TIME_LABEL_ONE_HOUR = "1 hour";
	public final static String TIME_LABEL_TWO_HOURS = "2 hours";
	public final static String TIME_LABEL_THREE_HOURS = "3 hours";
	public final static String TIME_LABEL_SIX_HOURS = "6 hours";
	public final static String TIME_LABEL_TWELVE_HOURS = "12 hours";
	public final static String TIME_LABEL_ONE_DAY = "1 day";

	// alarm request codes
	public final static int REQUEST_CODE_FETCH_ALARM = 9999;
	public final static int REQUEST_CODE_UPDATE_ALARM = 8888;

}

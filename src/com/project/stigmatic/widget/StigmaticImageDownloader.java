package com.project.stigmatic.widget;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import android.os.AsyncTask;

import com.project.mobile.utility.ConvertUtils;
import com.project.mobile.utility.Logger;
import com.project.stigmatic.configuration.ApplicationSettings;
import com.project.stigmatic.model.StigmaticModel;

/**
 * 
 * Image fetcher class
 * 
 */
public class StigmaticImageDownloader extends AsyncTask<Object, Integer, List<StigmaticModel>> {

	private List<StigmaticModel> stigmaticList;
	private IWidgetLoaderCallback<StigmaticModel> imageDownloadCallback;

	public StigmaticImageDownloader(List<StigmaticModel> stigmaticList, IWidgetLoaderCallback<StigmaticModel> imageDownloadCallback) {

		this.stigmaticList = stigmaticList;
		this.imageDownloadCallback = imageDownloadCallback;

	}

	/**
	 * If url exists, an input stream will be created and the photo will be read, or else an existing byte arrays will be converted to
	 * bitmaps.
	 */
	@Override
	protected List<StigmaticModel> doInBackground(Object... object) {

		URL url = null;
		HttpURLConnection connection = null;
		InputStream input = null;
		byte[] byteArray = null;

		for (StigmaticModel model : stigmaticList) {

			try {

				if (model.getPhoto() == null) {

					if (model.getPhotoUrl() != null) {

						url = new URL(model.getPhotoUrl());
						connection = (HttpURLConnection) url.openConnection();
						connection.setDoInput(true);
						connection.connect();
						if (model.getPlatform().equals(ApplicationSettings.PLATFORM_FACEBOOK)) {

							// Facebook redirects for profile picture
							connection.setInstanceFollowRedirects(true);
							url = new URL(connection.getHeaderField("Location"));
							connection = (HttpURLConnection) url.openConnection();
							connection.setDoInput(true);
							connection.connect();

						}
						input = connection.getInputStream();
						byteArray = ConvertUtils.getByteArrayFromInputStream(input);
						model.setPhoto(byteArray);

					}

				}

			} catch (Exception ex) {

				// possibly no connection or url problem..
				Logger.log(Logger.TYPE_ERROR, ex.getMessage(), getClass().getName());

			}

			try {

				if (model.getContentPhoto() == null) {

					if (model.getContentPhotoUrl() != null) {

						url = new URL(model.getContentPhotoUrl());
						connection = (HttpURLConnection) url.openConnection();
						connection.setDoInput(true);
						connection.connect();
						input = connection.getInputStream();
						byteArray = ConvertUtils.getByteArrayFromInputStream(input);
						model.setContentPhoto(byteArray);

					}

				}

			} catch (Exception e) {

				// possibly no connection or url problem..
				Logger.log(Logger.TYPE_ERROR, e.getMessage(), getClass().getName());

			}

		}

		return stigmaticList;

	}

	@Override
	protected void onPostExecute(List<StigmaticModel> resultList) {

		super.onPostExecute(resultList);
		// execute callback method
		imageDownloadCallback.onLoaderDataReceived(resultList);

	}

}

package com.project.stigmatic.widget;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.widget.Toast;

import com.project.mobile.communication.IGetCallback;
import com.project.mobile.communication.model.HttpResponseModel;
import com.project.mobile.integration.IParserCallback;
import com.project.mobile.integration.call.CallApi;
import com.project.mobile.integration.facebook.FacebookApi;
import com.project.mobile.integration.facebook.FacebookParser;
import com.project.mobile.integration.facebook.model.FacebookModel;
import com.project.mobile.integration.linkedin.LinkedInApi;
import com.project.mobile.integration.linkedin.LinkedInParser;
import com.project.mobile.integration.linkedin.model.CompanyUpdateModel;
import com.project.mobile.integration.linkedin.model.ConnectionUpdateModel;
import com.project.mobile.integration.linkedin.model.NetworkUpdateModel;
import com.project.mobile.integration.linkedin.model.PersonModel;
import com.project.mobile.integration.sms.SmsApi;
import com.project.mobile.utility.DateUtils;
import com.project.mobile.utility.Logger;
import com.project.mobile.utility.PhoneUtils;
import com.project.stigmatic.R;
import com.project.stigmatic.configuration.ApplicationSettings;
import com.project.stigmatic.model.StigmaticModel;
import com.project.stigmatic.persistence.DatabaseManager;

/**
 * 
 * Loads all data according to platform selections and also does required offline persistence operations for internal cache mechanism
 * 
 */
public class StigmaticWidgetLoader {

	// singleton instance
	private static final StigmaticWidgetLoader INSTANCE = new StigmaticWidgetLoader();

	// to prevent multiple refresh simultaneously..
	private AtomicBoolean LOAD_LOCKED;

	private StigmaticWidgetLoader() {

		LOAD_LOCKED = new AtomicBoolean(false);

	}

	public static StigmaticWidgetLoader getInstance() {

		return INSTANCE;

	}

	public void loadRemoteData(final SharedPreferences preferences, final Context context, final boolean fetch,
			final IWidgetLoaderCallback<StigmaticModel> stigmaticLoadCallback) {

		Logger.log(Logger.TYPE_INFO, "Remote Data Lock: " + LOAD_LOCKED.get(), getClass().getName());

		if (!LOAD_LOCKED.get()) {

			LOAD_LOCKED.set(true);

			final List<StigmaticModel> stigmaticList = new ArrayList<StigmaticModel>();

			if (PhoneUtils.hasNetworkConnection(context)) {

				downloadLinkedInData(preferences, context, fetch, new IWidgetLoaderCallback<NetworkUpdateModel>() {

					@Override
					public void onLoaderDataReceived(List<NetworkUpdateModel> resultList) {

						if (resultList != null) {

							List<StigmaticModel> linkedInResultList = new ArrayList<StigmaticModel>();

							StigmaticModel stigmaticModel = null;

							PersonModel person = null, newConnection = null;

							for (NetworkUpdateModel model : resultList) {

								stigmaticModel = new StigmaticModel();
								stigmaticModel.setPlatform(ApplicationSettings.PLATFORM_LINKEDIN);
								stigmaticModel.setDate(model.getTimestamp() > 0 ? DateUtils.getDateStringFromTimestamp(
										ApplicationSettings.DATE_FORMAT, model.getTimestamp()) : "");
								stigmaticModel.setTimestamp(model.getTimestamp());

								if (model instanceof ConnectionUpdateModel) {

									ConnectionUpdateModel connModel = (ConnectionUpdateModel) model;

									person = connModel.getPerson();

									if (person != null) {

										stigmaticModel.setUser(context.getResources().getString(R.string.hasNewConnection,
												person.getFirstName().concat(" ").concat(person.getLastName())));
										stigmaticModel.setHeader(connModel.getPerson().getHeadLine());
										stigmaticModel.setPhotoUrl(connModel.getPerson().getPictureUrl());

									}

									List<PersonModel> newConnectionList = connModel.getNewConnectionList();

									if (!newConnectionList.isEmpty()) {

										newConnection = newConnectionList.get(0);
										stigmaticModel.setContentPhotoUrl(newConnection.getPictureUrl());
										stigmaticModel.setMessage(newConnection.getFirstName().concat(" ")
												.concat(newConnection.getLastName()).concat("\n").concat(newConnection.getHeadLine()));
										stigmaticModel.setLink(newConnection.getProfileUrl());

									}

								} else if (model instanceof CompanyUpdateModel) {

									CompanyUpdateModel cmpyModel = (CompanyUpdateModel) model;

									stigmaticModel.setUser(cmpyModel.getName());
									stigmaticModel.setMessage(cmpyModel.getComment());
									stigmaticModel.setLink(cmpyModel.getLink());

								}

								linkedInResultList.add(stigmaticModel);

							}

							stigmaticList.addAll(linkedInResultList);

						}

						// Facebook
						downloadFacebookData(preferences, context, fetch, new IWidgetLoaderCallback<FacebookModel>() {

							@Override
							public void onLoaderDataReceived(List<FacebookModel> resultList) {

								if (resultList != null) {

									List<StigmaticModel> facebookResultList = new ArrayList<StigmaticModel>();
									StigmaticModel stigmaticModel = null;

									for (FacebookModel model : resultList) {

										stigmaticModel = new StigmaticModel();
										stigmaticModel.setUser(model.getUserName());
										stigmaticModel.setPlatform(ApplicationSettings.PLATFORM_FACEBOOK);
										stigmaticModel.setLink(model.getLink());
										stigmaticModel.setContentPhotoUrl(model.getContentPhotoUrl());
										stigmaticModel.setDate(model.getCreatedTime());
										stigmaticModel.setMessage(model.getMessage());
										stigmaticModel.setPhotoUrl(model.getUserPhotoUrl());
										stigmaticModel.setTimestamp(model.getTimestamp());
										stigmaticModel.setNativeAppLink(model.getFacebookLink());
										facebookResultList.add(stigmaticModel);

									}

									stigmaticList.addAll(facebookResultList);

								}

								if (!fetch) {

									new SaveStigmaticItemListTask(stigmaticList, context, stigmaticLoadCallback).execute();

								} else {

									new StigmaticImageDownloader(stigmaticList, new IWidgetLoaderCallback<StigmaticModel>() {

										@Override
										public void onLoaderDataReceived(List<StigmaticModel> resultList) {

											new SaveStigmaticItemListTask(stigmaticList, context, stigmaticLoadCallback).execute();

										}

									}).execute();

								}

							}

						});

					}

				});

			} else {

				new SaveStigmaticItemListTask(stigmaticList, context, stigmaticLoadCallback).execute();

			}

		}

	}

	public List<StigmaticModel> loadLocalData(SharedPreferences preferences, Context context) {

		boolean callSelected = preferences.getBoolean(ApplicationSettings.PLATFORM_CALL, false);
		boolean smsSelected = preferences.getBoolean(ApplicationSettings.PLATFORM_SMS, false);

		List<StigmaticModel> localDataList = new ArrayList<StigmaticModel>();

		long lastTimestamp = System.currentTimeMillis()
				- preferences.getLong(ApplicationSettings.KEY_SYNC_PERIOD, ApplicationSettings.TIME_ONE_DAY);

		List<StigmaticModel> list = null;

		// Call
		if (callSelected) {

			CallApi callApi = CallApi.getInstance();
			callApi.setDateFormat(ApplicationSettings.DATE_FORMAT);
			list = callApi.getCallList(context, lastTimestamp);

			if (!list.isEmpty())
				localDataList.addAll(list);

		}

		// Sms
		if (smsSelected) {

			SmsApi smsApi = SmsApi.getInstance();
			smsApi.setDateFormat(ApplicationSettings.DATE_FORMAT);
			list = smsApi.getMessageList(context, lastTimestamp);

			if (!list.isEmpty())
				localDataList.addAll(list);

		}

		return localDataList;

	}

	/**
	 * 
	 * Loads LinkedIn network updates and related images etc..
	 * 
	 */
	private void downloadLinkedInData(final SharedPreferences preferences, final Context context, final boolean fetch,
			final IWidgetLoaderCallback<NetworkUpdateModel> linkedInCallback) {

		boolean linkedInSelected = preferences.getBoolean(ApplicationSettings.PLATFORM_LINKEDIN, false);

		if (linkedInSelected) {

			String linkedInToken = preferences
					.getString(ApplicationSettings.PLATFORM_LINKEDIN + ApplicationSettings.KEY_ACCESS_TOKEN, null);

			final Long lastTimestamp = preferences.getLong(ApplicationSettings.PLATFORM_LINKEDIN + ApplicationSettings.KEY_TIMESTAMP,
					System.currentTimeMillis() - ApplicationSettings.TIME_ONE_DAY);

			// timestamp does not work due to crap Linkedin Developer API!
			LinkedInApi.getInstance().getFirstDegreeNetworkUpdates(linkedInToken, null, null, lastTimestamp, true, new IGetCallback() {

				@Override
				public void onDataReceived(HttpResponseModel response) {

					if (response.getStatusBoolean()) {

						LinkedInParser.getInstance().getLinkedInNetworkUpdateList(response.getResponse(),
								new IParserCallback<NetworkUpdateModel>() {

									@Override
									public void onParserDataReceived(List<NetworkUpdateModel> resultList) {

										SharedPreferences.Editor editor = preferences.edit();

										if (lastTimestamp >= 0) {

											// timestamp does
											// not work due to
											// crap Linkedin
											// Developer API! We
											// need to check
											// every
											// item!?
											List<NetworkUpdateModel> freshList = new ArrayList<NetworkUpdateModel>();

											if (!resultList.isEmpty()) {

												Long newLastTimestamp = null, iteratorTimestamp = null;

												for (NetworkUpdateModel model : resultList) {

													iteratorTimestamp = model.getTimestamp();

													if (iteratorTimestamp > lastTimestamp) {

														if (resultList.indexOf(model) == 0) {

															newLastTimestamp = model.getTimestamp();
															editor.putLong(ApplicationSettings.PLATFORM_LINKEDIN
																	+ ApplicationSettings.KEY_TIMESTAMP, newLastTimestamp);

															editor.commit();

														}

														freshList.add(model);

													} else {

														break;

													}

												}

											}

											linkedInCallback.onLoaderDataReceived(freshList);

										} else {

											if (!resultList.isEmpty()) {

												editor.putLong(ApplicationSettings.PLATFORM_LINKEDIN + ApplicationSettings.KEY_TIMESTAMP,
														resultList.get(0).getTimestamp());

												editor.commit();

											}

											linkedInCallback.onLoaderDataReceived(resultList);

										}

										if (!fetch) {

											Toast.makeText(
													context,
													context.getResources().getString(R.string.updateSuccess,
															context.getResources().getString(R.string.linkedin)), Toast.LENGTH_SHORT)
													.show();

										}

									}

								});

					} else {

						// Possibly timeout happened..
						LOAD_LOCKED.set(false);

						if (!fetch) {

							Toast.makeText(
									context,
									context.getResources().getString(R.string.updateFailed,
											context.getResources().getString(R.string.linkedin)), Toast.LENGTH_SHORT).show();

						}

						linkedInCallback.onLoaderDataReceived(null);

					}

				}

			});

		} else {

			linkedInCallback.onLoaderDataReceived(null);

		}

	}

	/**
	 * 
	 * Loads Facebook content
	 * 
	 */
	private void downloadFacebookData(final SharedPreferences preferences, final Context context, final boolean fetch,
			final IWidgetLoaderCallback<FacebookModel> facebookCallback) {

		boolean facebookSelected = preferences.getBoolean(ApplicationSettings.PLATFORM_FACEBOOK, false);

		if (facebookSelected) {

			String facebookToken = preferences
					.getString(ApplicationSettings.PLATFORM_FACEBOOK + ApplicationSettings.KEY_ACCESS_TOKEN, null);

			long lastTimestamp = preferences.getLong(ApplicationSettings.PLATFORM_FACEBOOK + ApplicationSettings.KEY_TIMESTAMP,
					(System.currentTimeMillis() - ApplicationSettings.TIME_ONE_DAY) / 1000L);

			FacebookApi.getInstance().getSelfProfile(facebookToken, lastTimestamp, true, new IGetCallback() {

				@Override
				public void onDataReceived(HttpResponseModel response) {

					if (response.getStatusBoolean()) {

						FacebookParser.getInstance().getFacebookTimelineUpdateList(response.getResponse(),
								new IParserCallback<FacebookModel>() {

									@Override
									public void onParserDataReceived(List<FacebookModel> resultList) {
										// Calendar cal =
										// Calendar.getInstance();
										// //cal.setTimeZone(TimeZone.getTimeZone("+0000"));
										Long timestamp = System.currentTimeMillis() / 1000L;

										SharedPreferences.Editor editor = preferences.edit();
										editor.putLong(ApplicationSettings.PLATFORM_FACEBOOK + ApplicationSettings.KEY_TIMESTAMP, timestamp);
										editor.commit();
										facebookCallback.onLoaderDataReceived(resultList);

										if (!fetch) {

											Toast.makeText(
													context,
													context.getResources().getString(R.string.updateSuccess,
															context.getResources().getString(R.string.facebook)), Toast.LENGTH_SHORT)
													.show();

										}

									}

								});

					} else {

						// Possibly timeout happened..
						LOAD_LOCKED.set(false);

						if (!fetch) {

							Toast.makeText(
									context,
									context.getResources().getString(R.string.updateFailed,
											context.getResources().getString(R.string.facebook)), Toast.LENGTH_SHORT).show();

						}

						facebookCallback.onLoaderDataReceived(null);

					}

				}

			});

		} else {

			facebookCallback.onLoaderDataReceived(null);

		}

	}

	/**
	 * 
	 * Persisting new items to our database for pre caching and offline module
	 * 
	 */
	private final class SaveStigmaticItemListTask extends AsyncTask<Object, Object, Object> {

		private DatabaseManager databaseHandler = null;
		private List<StigmaticModel> newList;
		private IWidgetLoaderCallback<StigmaticModel> stigmaticLoaderCallback;

		public SaveStigmaticItemListTask(List<StigmaticModel> newList, Context context,
				IWidgetLoaderCallback<StigmaticModel> stigmaticLoaderCallback) {

			this.newList = newList;
			databaseHandler = DatabaseManager.getInstance(context);
			this.stigmaticLoaderCallback = stigmaticLoaderCallback;

		}

		@Override
		protected Cursor doInBackground(Object... params) {

			databaseHandler.insertModelListAfterDeletingOutOfSync(newList);

			return null;

		}

		// close the database connection and open the lock
		@Override
		protected void onPostExecute(Object result) {

			LOAD_LOCKED.set(false);
			stigmaticLoaderCallback.onLoaderDataReceived(newList);

		}

	}

}
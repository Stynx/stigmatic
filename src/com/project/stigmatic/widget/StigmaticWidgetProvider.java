package com.project.stigmatic.widget;

import java.util.List;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.project.mobile.utility.Logger;
import com.project.stigmatic.R;
import com.project.stigmatic.activity.StigmaticActivity;
import com.project.stigmatic.configuration.ApplicationSettings;
import com.project.stigmatic.model.StigmaticModel;

/**
 * 
 * Stigmatic widget provider that has constant update method in it
 * 
 */
public class StigmaticWidgetProvider extends AppWidgetProvider {

	private static final String CLICK_ACTION = "com.project.stigmatic.CLICK_ACTION";
	public static final String SELECTED_ITEM = "com.project.stigmatic.SELECTED_ITEM";
	private static final String REFRESH_ACTION = "com.project.stigmatic.REFRESH_ACTION";
	private static final String UPDATE_ACTION = "com.project.stigmatic.UPDATE_ACTION";
	private static final String FETCH_DATA = "com.project.stigmatic.FETCH_DATA";
	public static final String CONFIGURE_WIDGET = "com.project.stigmatic.CONFIGURE_WIDGET";
	private static final String OPEN_SETTINGS = "com.project.stigmatic.OPEN_SETTINGS";
	private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
	private static final String CALL_RECEIVED = "android.intent.action.PHONE_STATE";

	private static PendingIntent fetchService = null;
	private static PendingIntent updateService = null;

	private static boolean ring = false;
	private static boolean callReceived = false;

	/**
	 * This method works after user puts the widget to homescreen.
	 */
	@Override
	public void onEnabled(Context context) {

		Logger.log(Logger.TYPE_INFO, "On Enabled..", getClass().getName());

		prepareUpdateAlarm(context);
		prepareFetchAlarm(context);

		final SharedPreferences preferences = context.getSharedPreferences(ApplicationSettings.SHARED_PREFERENCES_NAME,
				Context.MODE_MULTI_PROCESS);

		if (preferences.getBoolean(ApplicationSettings.APPLICATION_COLD_START, true)) {

			SharedPreferences.Editor editor = preferences.edit();
			editor.putBoolean(ApplicationSettings.APPLICATION_COLD_START, false);
			editor.commit();

			onRefreshWidget(context);

		} else {

			onPushWidgetUpdate(context);

		}

		super.onEnabled(context);

	}

	/**
	 * When onDelete triggers, if there is at least one widget on homescreen, we update it with latest database items.
	 */
	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {

		if (appWidgetIds != null && appWidgetIds.length > 0)
			onPushWidgetUpdate(context);

		super.onDeleted(context, appWidgetIds);

	}

	/**
	 * Fetching alarm. Its main purpose is to get data beyond updating process.
	 * 
	 * @param context
	 */
	private void prepareFetchAlarm(Context context) {
		// prepare Alarm Service to trigger fetching
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent fetchIntent = new Intent(context, StigmaticWidgetProvider.class);
		fetchIntent.setAction(FETCH_DATA);
		fetchService = PendingIntent.getBroadcast(context, ApplicationSettings.REQUEST_CODE_FETCH_ALARM, fetchIntent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		SharedPreferences preferences = context.getSharedPreferences(ApplicationSettings.SHARED_PREFERENCES_NAME,
				Context.MODE_MULTI_PROCESS);
		long updateTimeInterval = preferences.getLong(ApplicationSettings.KEY_UPDATE_INTERVAL, ApplicationSettings.TIME_THIRTY_MINUTES);
		long fetchInterval = updateTimeInterval / 3 + 50000;
		Logger.log(Logger.TYPE_WARN, "Fetch alarm started with interval: " + fetchInterval, getClass().getName());
		alarmManager.setRepeating(AlarmManager.RTC, System.currentTimeMillis() + fetchInterval, fetchInterval, fetchService);

	}

	/**
	 * Main updating timer. We simply used this timer to let user select interval in settings. We disabled default widget updater with
	 * setting zero to related field.
	 * 
	 * @param context
	 */
	private void prepareUpdateAlarm(Context context) {

		// prepare Alarm Service to trigger updating
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent updateIntent = new Intent(context, StigmaticWidgetProvider.class);
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		ComponentName appWidget = new ComponentName(context.getPackageName(), StigmaticWidgetProvider.class.getName());
		int[] appWidgetIds = appWidgetManager.getAppWidgetIds(appWidget);
		updateIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
		updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
		updateIntent.putExtra(UPDATE_ACTION, true);
		updateService = PendingIntent.getBroadcast(context, ApplicationSettings.REQUEST_CODE_UPDATE_ALARM, updateIntent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		SharedPreferences preferences = context.getSharedPreferences(ApplicationSettings.SHARED_PREFERENCES_NAME,
				Context.MODE_MULTI_PROCESS);
		long updateTimeInterval = preferences.getLong(ApplicationSettings.KEY_UPDATE_INTERVAL, ApplicationSettings.TIME_THIRTY_MINUTES);
		Logger.log(Logger.TYPE_WARN, "Update alarm started with interval: " + updateTimeInterval, getClass().getName());
		alarmManager.setRepeating(AlarmManager.RTC, System.currentTimeMillis() + updateTimeInterval, updateTimeInterval, updateService);

	}

	/**
	 * This method works when user removes all widget instances from homescreen. We cancel all alarms.
	 */
	@Override
	public void onDisabled(final Context context) {

		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

		Intent fetchIntent = new Intent(context, StigmaticWidgetProvider.class);
		fetchService = PendingIntent.getBroadcast(context, ApplicationSettings.REQUEST_CODE_FETCH_ALARM, fetchIntent,
				PendingIntent.FLAG_UPDATE_CURRENT);

		alarmManager.cancel(fetchService);

		Intent updateIntent = new Intent(context, StigmaticWidgetProvider.class);
		updateService = PendingIntent.getBroadcast(context, ApplicationSettings.REQUEST_CODE_UPDATE_ALARM, updateIntent,
				PendingIntent.FLAG_UPDATE_CURRENT);

		alarmManager.cancel(updateService);

		super.onDisabled(context);

	}

	/**
	 * Broadcast receiver method. We catch broadcasts here such as refresh requests or click actions
	 */
	@Override
	public void onReceive(final Context context, Intent intent) {

		if (intent.getAction().equals(FETCH_DATA) && anyWidgetOnScreen(context)) {

			onFetchData(context);

		} else if (intent.getAction().equals(CLICK_ACTION)) {

			onClick(intent, context);

		} else if (intent.getBooleanExtra(REFRESH_ACTION, false) && intent.getAction().equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)) {

			onRefreshWidget(context);

		} else if (intent.getBooleanExtra(CONFIGURE_WIDGET, false) && intent.getAction().equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)
				&& anyWidgetOnScreen(context)) {

			onConfigureWidget(context);

		} else if (intent.getBooleanExtra(UPDATE_ACTION, false) && intent.getAction().equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)) {

			onTimerUpdate(context);

		} else if (intent.getAction().equals(OPEN_SETTINGS)) {

			onOpenSettings(context);

		} else if (intent.getAction().equals(SMS_RECEIVED)) {

			onSmsReceived(context);

		} else if (intent.getAction().equals(CALL_RECEIVED)) {

			onIncomingCall(intent, context);

		}

		super.onReceive(context, intent);

	}

	private void onTimerUpdate(Context context) {

		Logger.log(Logger.TYPE_INFO, "Periodic update started..", getClass().getName());
		onPushWidgetUpdate(context);

	}

	private void onOpenSettings(Context context) {

		// when user touches the settings button on the widget, we open main activity
		Logger.log(Logger.TYPE_INFO, "Opening main activity", getClass().getName());
		Intent settingsIntent = new Intent(context, StigmaticActivity.class);
		settingsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(settingsIntent);

	}

	private void onClick(Intent intent, Context context) {

		// when user clicks an item on the widget, we open it in native app or mobile web browser
		String[] extraArray = intent.getStringArrayExtra(SELECTED_ITEM);

		String platform = extraArray[0];
		String link = extraArray[1];
		String nativeAppLink = extraArray[2];

		Logger.log(Logger.TYPE_INFO, "Link: " + link, getClass().getName());

		if (platform.equals(ApplicationSettings.PLATFORM_CALL)) {

			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setData(Uri.parse(link));
			callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(callIntent);

		} else if (platform.equals(ApplicationSettings.PLATFORM_SMS)) {

			Intent smsIntent = new Intent(Intent.ACTION_VIEW);
			smsIntent.setData(Uri.parse(link));
			smsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(smsIntent);

		} else if (platform.equals(ApplicationSettings.PLATFORM_FACEBOOK)) {

			Intent facebookIntent = new Intent(Intent.ACTION_VIEW);

			try {

				facebookIntent.setData(Uri.parse(nativeAppLink));
				Logger.log(Logger.TYPE_INFO, "Native App Link: " + nativeAppLink, getClass().getName());
				facebookIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(facebookIntent);

			} catch (Exception e) {

				facebookIntent.setData(Uri.parse(link));
				facebookIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(facebookIntent);

			}

		} else if (platform.equals(ApplicationSettings.PLATFORM_LINKEDIN) && link != null) {

			Intent linkedinIntent = new Intent(Intent.ACTION_VIEW);
			linkedinIntent.setData(Uri.parse(link));
			linkedinIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(linkedinIntent);

		}

	}

	private void onIncomingCall(Intent intent, Context context) {

		// get the current Phone State
		String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

		if (state == null)
			return;

		// if phone state "Ringing"
		if (state.equals(TelephonyManager.EXTRA_STATE_RINGING))
			ring = true;

		// if incoming call is received
		if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK))
			callReceived = true;

		// if phone is Idle
		if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {

			// if phone was ringing(ring=true) and not received(callReceived=false) , then it is a missed call
			if (ring == true && callReceived == false) {

				onPushWidgetUpdate(context);
				ring = false;
				callReceived = false;

			}

		}

	}

	/**
	 * Prepares alarms with new time settings. Then refreshes the widget with new content
	 * 
	 * @param context
	 *            Current context
	 */
	private void onConfigureWidget(Context context) {

		Logger.log(Logger.TYPE_INFO, "Configuring Widget.", getClass().getName());
		onRefreshWidget(context);
		prepareUpdateAlarm(context);
		prepareFetchAlarm(context);

	}

	private void onSmsReceived(final Context context) {

		Logger.log(Logger.TYPE_INFO, "Sms received..", getClass().getName());
		onPushWidgetUpdate(context);

	}

	/**
	 * Refreshes widget with new online and offline content
	 * 
	 * @param context
	 *            Current context
	 */
	private void onRefreshWidget(final Context context) {

		final SharedPreferences preferences = context.getSharedPreferences(ApplicationSettings.SHARED_PREFERENCES_NAME,
				Context.MODE_MULTI_PROCESS);

		final AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		ComponentName appWidget = new ComponentName(context.getPackageName(), StigmaticWidgetProvider.class.getName());
		final int[] appWidgetIds = appWidgetManager.getAppWidgetIds(appWidget);

		showLoadingToast(context, null);

		StigmaticWidgetLoader.getInstance().loadRemoteData(preferences, context, false, new IWidgetLoaderCallback<StigmaticModel>() {

			@Override
			public void onLoaderDataReceived(List<StigmaticModel> resultList) {

				appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.widgetListView);

			}

		});

	}

	/**
	 * Fetches all data before update triggers. This includes images also.
	 * 
	 * @param context
	 *            Current context
	 */
	private void onFetchData(final Context context) {

		SharedPreferences preferences = context.getSharedPreferences(ApplicationSettings.SHARED_PREFERENCES_NAME,
				Context.MODE_MULTI_PROCESS);

		Logger.log(Logger.TYPE_INFO, "Fetching data now.", getClass().getName());

		StigmaticWidgetLoader.getInstance().loadRemoteData(preferences, context, true, new IWidgetLoaderCallback<StigmaticModel>() {

			@Override
			public void onLoaderDataReceived(List<StigmaticModel> resultList) {

				// Nothing to do here..

			}

		});

	}

	/**
	 * Utility method for showing toasts. If no message is set as parameter, loading message will be shown
	 * 
	 * @param context
	 *            Current context
	 * @param customMessage
	 *            Custom toast message
	 */
	private void showLoadingToast(Context context, String customMessage) {

		Toast toast = Toast.makeText(context, customMessage == null ? context.getResources().getString(R.string.loading) : customMessage,
				Toast.LENGTH_SHORT);
		toast.show();

	}

	/**
	 * Notifies widget to update itself
	 * 
	 * @param context
	 *            Current context
	 */
	public void onPushWidgetUpdate(final Context context) {

		showLoadingToast(context, "Updating now..");

		final AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		ComponentName appWidget = new ComponentName(context.getPackageName(), StigmaticWidgetProvider.class.getName());
		final int[] appWidgetIds = appWidgetManager.getAppWidgetIds(appWidget);

		appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.widgetListView);

	}

	/**
	 * Called for every update of the widget. Contains the ids of appWidgetIds for which an update is needed. Note that this may be all of
	 * the AppWidget instances for this provider, or just a subset of them, as stated in the methods JavaDoc. For example if more than one
	 * widget is added to the home screen, only the last one changes (until reinstall).
	 */
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

		Intent clickIntent = null, intent = null, refreshIntent = null, settingsIntent = null;
		PendingIntent pendingIntent = null;

		// update each of the widgets with the remote adapter
		for (int i = 0; i < appWidgetIds.length; ++i) {

			// Here we setup the intent which points to the StackViewService which will
			// provide the views for this collection.
			intent = new Intent(context, StigmaticWidgetService.class);
			intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetIds[i]);

			// When intents are compared, the extras are ignored, so we need to embed the extras
			// into the data so that the extras will not be ignored.
			intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

			// Here we setup the a pending intent template. Individuals items of a collection
			// cannot setup their own pending intents, instead, the collection as a whole can
			// setup a pending intent template, and the individual items can set a fillInIntent
			// to create unique before on an item to item basis.
			clickIntent = new Intent(context, StigmaticWidgetProvider.class);
			clickIntent.setAction(StigmaticWidgetProvider.CLICK_ACTION);
			clickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetIds[i]);
			intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
			pendingIntent = PendingIntent.getBroadcast(context, 0, clickIntent, PendingIntent.FLAG_UPDATE_CURRENT);

			refreshIntent = new Intent(context, StigmaticWidgetProvider.class);
			refreshIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
			refreshIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
			refreshIntent.putExtra(REFRESH_ACTION, true);

			settingsIntent = new Intent(context, StigmaticWidgetProvider.class);
			settingsIntent.setAction(OPEN_SETTINGS);

			RemoteViews remoteView = new RemoteViews(context.getPackageName(), R.layout.stigmatic_widget_layout);
			remoteView.setRemoteAdapter(R.id.widgetListView, intent);
			// The empty view is displayed when the collection has no items. It should be a sibling
			// of the collection view.
			remoteView.setEmptyView(R.id.widgetListView, R.id.widgetEmptyView);
			remoteView.setPendingIntentTemplate(R.id.widgetListView, pendingIntent);
			pendingIntent = PendingIntent.getBroadcast(context, 0, refreshIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			remoteView.setOnClickPendingIntent(R.id.refreshImageView, pendingIntent);
			pendingIntent = PendingIntent.getBroadcast(context, 0, settingsIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			remoteView.setOnClickPendingIntent(R.id.settingsImageView, pendingIntent);

			appWidgetManager.updateAppWidget(appWidgetIds[i], remoteView);

		}

		super.onUpdate(context, appWidgetManager, appWidgetIds);

	}

	/**
	 * Utility method to check if any widget instance exists in the homescreen
	 * 
	 * @param context
	 *            Current context
	 * @return
	 */
	private boolean anyWidgetOnScreen(Context context) {

		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		ComponentName appWidget = new ComponentName(context.getPackageName(), StigmaticWidgetProvider.class.getName());
		int[] appWidgetIds = appWidgetManager.getAppWidgetIds(appWidget);

		if (appWidgetIds != null && appWidgetIds.length > 0)
			return true;

		return false;

	}

}

package com.project.stigmatic.widget;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.project.mobile.utility.ConvertUtils;
import com.project.mobile.utility.Logger;
import com.project.stigmatic.R;
import com.project.stigmatic.configuration.ApplicationSettings;
import com.project.stigmatic.model.StigmaticModel;
import com.project.stigmatic.persistence.DatabaseManager;

/**
 * 
 * Widget service class to use collections
 * 
 */
public class StigmaticWidgetService extends RemoteViewsService {

	// creates factory
	@Override
	public RemoteViewsFactory onGetViewFactory(Intent intent) {

		return new StigmaticRemoteViewsFactory(this.getApplicationContext(), intent);

	}

	// factory instance to draw list items for collections
	class StigmaticRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {

		private Context context;
		private List<StigmaticModel> itemList;

		public StigmaticRemoteViewsFactory(Context context, Intent intent) {

			this.context = context;

		}

		public void onCreate() {

			// we use heavy loading so we can not use this method because there will be anr message after 20 seconds..

		}

		public void onDestroy() {

			// In onDestroy() you should tear down anything that was setup for your data source, eg. cursors, connections, etc.
			getItemList().clear();

		}

		public int getCount() {

			return getItemList().size();

		}

		public RemoteViews getViewAt(int position) {

			StigmaticModel item = null;

			// position will always range from 0 to getCount() - 1.

			try {

				item = getItemList().get(position);

			} catch (IndexOutOfBoundsException ex) {
			}

			// we construct a remote views item based on our widget item xml file, and set the
			// text based on the position.
			RemoteViews remoteView = new RemoteViews(context.getPackageName(), R.layout.stigmatic_widget_item_layout);

			if (item != null) {

				remoteView.setTextViewText(R.id.userTextView, item.getUser());
				remoteView.setTextViewText(R.id.messageTextView, item.getMessage());
				remoteView.setTextViewText(R.id.dateTextView, item.getDate());

				HttpURLConnection connection = null;
				InputStream input = null;
				Bitmap bitmap = null;
				byte[] byteArray = null;
				URL url = null;

				try {

					// processing intensive operations are handled here
					if (item.getPhoto() == null) {

						if (item.getPhotoUrl() != null) {

							url = new URL(item.getPhotoUrl());
							connection = (HttpURLConnection) url.openConnection();
							connection.setDoInput(true);
							connection.connect();
							if (item.getPlatform().equals(ApplicationSettings.PLATFORM_FACEBOOK)) {

								// Facebook redirects for profile picture
								connection.setInstanceFollowRedirects(true);
								url = new URL(connection.getHeaderField("Location"));
								connection = (HttpURLConnection) url.openConnection();
								connection.setDoInput(true);
								connection.connect();

							}
							input = connection.getInputStream();
							byteArray = ConvertUtils.getByteArrayFromInputStream(input);
							bitmap = ConvertUtils.getBitmapFromByteArray(byteArray);
							item.setPhoto(byteArray);
							item.setPhotoBitmap(bitmap);
							item.setImageSavedBefore(false);

						} else {

							item.setImageSavedBefore(true);

						}

					} else {

						item.setImageSavedBefore(true);
						item.setPhotoBitmap(ConvertUtils.getBitmapFromByteArray(item.getPhoto()));

					}

				} catch (Exception ex) {

					// possibly no connection or url problem..
					item.setImageSavedBefore(true);
					Logger.log(Logger.TYPE_ERROR, ex.getMessage(), getClass().getName());

				}

				try {

					if (item.getContentPhoto() == null) {

						if (item.getContentPhotoUrl() != null) {

							url = new URL(item.getContentPhotoUrl());
							connection = (HttpURLConnection) url.openConnection();
							connection.setDoInput(true);
							connection.connect();
							input = connection.getInputStream();
							byteArray = ConvertUtils.getByteArrayFromInputStream(input);
							bitmap = ConvertUtils.getBitmapFromByteArray(byteArray);
							item.setContentPhoto(byteArray);
							item.setContentPhotoBitmap(bitmap);
							item.setImageSavedBefore(false);

						} else {

							item.setImageSavedBefore(true);

						}

					} else {

						item.setImageSavedBefore(true);
						item.setContentPhotoBitmap(ConvertUtils.getBitmapFromByteArray(item.getContentPhoto()));

					}

				} catch (Exception e) {

					// possibly no connection or url problem..
					item.setImageSavedBefore(true);
					Logger.log(Logger.TYPE_ERROR, e.getMessage(), getClass().getName());

				}

				if (item.getPhotoBitmap() != null) {

					remoteView.setImageViewBitmap(R.id.userImageView, item.getPhotoBitmap());

				} else {

					remoteView.setImageViewResource(R.id.userImageView, R.drawable.user);

				}

				if (item.getContentPhotoBitmap() != null) {

					remoteView.setImageViewBitmap(R.id.contentImageView, item.getContentPhotoBitmap());
					remoteView.setViewVisibility(R.id.contentImageView, View.VISIBLE);

				} else {

					remoteView.setImageViewResource(R.id.contentImageView, R.drawable.transparent);
					remoteView.setViewVisibility(R.id.contentImageView, View.GONE);

				}

				int drawableId = -1;

				if (item.getPlatform().equals(ApplicationSettings.PLATFORM_LINKEDIN)) {

					drawableId = R.drawable.linkedin;

				} else if (item.getPlatform().equals(ApplicationSettings.PLATFORM_FACEBOOK)) {

					drawableId = R.drawable.facebook;

				} else if (item.getPlatform().equals(ApplicationSettings.PLATFORM_SMS)) {

					drawableId = R.drawable.sms;

				} else if (item.getPlatform().equals(ApplicationSettings.PLATFORM_CALL)) {

					drawableId = R.drawable.call;

				}

				remoteView.setImageViewResource(R.id.platformImageView, drawableId);

				// next, we set a fill-intent which will be used to fill-in the pending intent template
				// which is set on the collection view in WidgetProvider.
				Bundle extras = new Bundle();
				extras.putStringArray(StigmaticWidgetProvider.SELECTED_ITEM,
						new String[] { item.getPlatform(), item.getLink(), item.getNativeAppLink() });

				Intent fillInIntent = new Intent();
				fillInIntent.putExtras(extras);
				remoteView.setOnClickFillInIntent(R.id.widgetItemLayout, fillInIntent);

				// after new photos are fetched, they are persisted under database to lower data consumption

				if (!item.isImageSavedBefore()) {

					DatabaseManager.getInstance(context).updateModel(item);

				}

			}

			// return the remote view object.
			return remoteView;

		}

		public void onDataSetChanged() {

			// This is triggered when you call AppWidgetManager notifyAppWidgetViewDataChanged
			// on the collection view corresponding to this factory. You can do heaving lifting in
			// here, synchronously. For example, if you need to process an image, fetch something
			// from the network, etc., it is ok to do it here, synchronously. The widget will remain
			// in its current state while work is being done here, so you don't need to worry about
			// locking up the widget.

			// Refresh the list

			DatabaseManager databaseManager = DatabaseManager.getInstance(context);
			SharedPreferences preferences = context.getSharedPreferences(ApplicationSettings.SHARED_PREFERENCES_NAME,
					Context.MODE_MULTI_PROCESS);
			Long lastTimestamp = System.currentTimeMillis()
					- preferences.getLong(ApplicationSettings.KEY_SYNC_PERIOD, System.currentTimeMillis()
							- ApplicationSettings.TIME_ONE_DAY);
			itemList = databaseManager.getModelListByCriteria(lastTimestamp, false);
			getItemList().addAll(StigmaticWidgetLoader.getInstance().loadLocalData(preferences, context));
			Collections.sort(getItemList(), StigmaticModel.TimestampComparator);

		}

		public RemoteViews getLoadingView() {

			// You can create a custom loading view (for instance when getViewAt() is slow.) If you return null here, you will get the
			// default loading view.
			return null;

		}

		public int getViewTypeCount() {

			return 1;

		}

		public long getItemId(int position) {

			return position;

		}

		public boolean hasStableIds() {

			return true;

		}

		public List<StigmaticModel> getItemList() {

			if (itemList == null)
				itemList = new ArrayList<StigmaticModel>();

			return itemList;

		}

	}

}

package com.project.stigmatic.widget;

import java.util.List;

/**
 * 
 * Callback for widget loader results.
 * 
 */
public interface IWidgetLoaderCallback<T> {

	/**
	 * Called when a widget loader loads all required data
	 */
	void onLoaderDataReceived(List<T> resultList);

}

package com.project.stigmatic.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.webkit.WebView;

/**
 * 
 * Custom WebView for platform login operations
 * 
 */
public class PlatformWebView extends WebView {

	public PlatformWebView(Context context) {

		this(context, null);

	}

	public PlatformWebView(Context context, AttributeSet attrs) {

		this(context, attrs, 0);

	}

	public PlatformWebView(Context context, AttributeSet attrs, int defStyle) {

		super(context, attrs, defStyle);

	}

	@Override
	public boolean onCheckIsTextEditor() {

		return true;

	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {

		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_UP:

			if (!hasFocus()) {

				requestFocus();

			}

			break;

		}

		return super.onTouchEvent(ev);

	}

}

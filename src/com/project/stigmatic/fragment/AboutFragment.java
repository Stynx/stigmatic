package com.project.stigmatic.fragment;

import java.io.InputStream;

import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.project.mobile.utility.Logger;
import com.project.stigmatic.R;
import com.project.stigmatic.configuration.ApplicationSettings;

/**
 * About tab implementation. Informative data such as change log, application name or authors reside here.
 * 
 */
public class AboutFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.stigmatic_about_layout, container, false);

		try {

			String versionName = "";
			PackageInfo pinfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
			versionName = pinfo.versionName;
			TextView aboutTextView = (TextView) rootView.findViewById(R.id.about_text_view);
			InputStream is = getActivity().getAssets().open(ApplicationSettings.ABOUT_HTML_NAME);
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();
			String aboutText = "<h1>" + getResources().getString(R.string.app_name) + ", Version " + versionName + "</h1>";
			String aboutDetail = new String(buffer);
			StringBuffer aboutBuffer = new StringBuffer(aboutText);
			aboutBuffer.append(aboutDetail);
			aboutTextView.setText(Html.fromHtml(aboutBuffer.toString()));

		} catch (Exception e) {

			Logger.log(Logger.TYPE_ERROR, e.getMessage(), getClass().getName());

		}

		return rootView;

	}

}

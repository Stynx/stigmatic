package com.project.stigmatic.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;

import com.project.stigmatic.R;
import com.project.stigmatic.activity.StigmaticActivity;

/**
 * 
 * Settings tab implementation. Platform logins or advanced settings reside here
 * 
 */
public class SettingsFragment extends Fragment {

	private TableLayout settingsTableLayout;
	private boolean pageInit;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.stigmatic_settings_layout, container, false);

		// using internal caching in order not to create all rows again..

		TableLayout tableLayout = (TableLayout) rootView.findViewById(R.id.platformTableLayout);

		if (this.settingsTableLayout != null) {

			int cachedChildCount = this.settingsTableLayout.getChildCount();

			List<View> cachedItemList = new ArrayList<View>();

			for (int i = 0; i < cachedChildCount; i++) {

				cachedItemList.add(this.settingsTableLayout.getChildAt(i));

			}

			this.settingsTableLayout.removeAllViews();

			for (int i = 0; i < cachedChildCount; i++) {

				tableLayout.addView(cachedItemList.get(i));

			}

		}

		this.settingsTableLayout = tableLayout;

		if (pageInit) {

			FragmentActivity foregroundActivity = getActivity();

			((StigmaticActivity) foregroundActivity).setSettingsFragmentViews();

			pageInit = false;

		}

		return rootView;

	}

	public TableLayout getSettingsTableLayout() {

		return settingsTableLayout;

	}

	public void setPageInit(boolean pageInit) {

		this.pageInit = pageInit;

	}

}

package com.project.stigmatic.activity;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.project.mobile.communication.IGetCallback;
import com.project.mobile.communication.IRestTaskCallback;
import com.project.mobile.communication.PostTask;
import com.project.mobile.communication.model.HttpRequestModel;
import com.project.mobile.communication.model.HttpResponseModel;
import com.project.mobile.integration.facebook.FacebookApi;
import com.project.mobile.integration.linkedin.LinkedInApi;
import com.project.mobile.utility.ConvertUtils;
import com.project.mobile.utility.Logger;
import com.project.stigmatic.R;
import com.project.stigmatic.adapter.TabPagerAdapter;
import com.project.stigmatic.configuration.ApplicationSettings;
import com.project.stigmatic.fragment.SettingsFragment;
import com.project.stigmatic.persistence.DatabaseManager;
import com.project.stigmatic.view.PlatformWebView;
import com.project.stigmatic.widget.StigmaticWidgetProvider;

/**
 * 
 * Main activity of Stigmatic application.
 * 
 */
public class StigmaticActivity extends FragmentActivity implements TabListener {

	// pager for detecting
	private ViewPager viewPager;

	// tab adapter for coordinating tab change process
	private TabPagerAdapter tabPagerAdapter;

	// application action bar
	private ActionBar actionBar;

	// tablelayout consists of platforms
	private TableLayout settingsTableLayout;

	// current tab position for internal cache mechanism
	private int currentTabIndex;

	// is page loaded for first time
	private boolean pageInit;

	// web view for platform logins
	private WebView platformWebView;

	// progress dialog for platform logins
	private AlertDialog platformDialog;

	// selected platform key
	private String selectedPlatform;

	// dialog control on various login situations
	private boolean closeDialog;

	// saves last clicked platform status image view
	private ImageView lastClickedPlatformStatusImageView;

	// application main progress dialog
	private ProgressDialog progressDialog;

	// custom dialog to play tutorial
	private AlertDialog videoDialog;

	// Video view to play tutorial
	private VideoView videoView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.stigmatic_app_layout);

		// action bar, pager and tab adapter initialization
		viewPager = (ViewPager) findViewById(R.id.stigmaticPager);
		actionBar = getActionBar();
		tabPagerAdapter = new TabPagerAdapter(getSupportFragmentManager());
		viewPager.setAdapter(tabPagerAdapter);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.addTab(actionBar.newTab().setText(getResources().getString(R.string.settings)).setTabListener(this),
				ApplicationSettings.INDEX_FRAGMENT_SETTINGS);
		actionBar.addTab(actionBar.newTab().setText(getResources().getString(R.string.about)).setTabListener(this),
				ApplicationSettings.INDEX_FRAGMENT_ABOUT);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		platformDialog = alertDialogBuilder.create();

		View videoLayout = getLayoutInflater().inflate(R.layout.stigmatic_video_layout, null);

		videoView = (VideoView) videoLayout.findViewById(R.id.videoView);

		videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

			public void onCompletion(MediaPlayer mp) {

				videoDialog.cancel();

			}

		});

		alertDialogBuilder.setView(videoLayout);

		videoDialog = alertDialogBuilder.create();

		// on swiping the viewpager, makes respective tab selected
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int tabIndex) {

				currentTabIndex = tabIndex;
				// on changing the page makes tab selected
				actionBar.setSelectedNavigationItem(tabIndex);

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}

		});

		// platform web view initialization
		platformWebView = new PlatformWebView(this);
		platformWebView.setFocusable(true);
		platformWebView.setFocusableInTouchMode(true);

		// Set a custom web view client
		platformWebView.setWebViewClient(new WebViewClient() {

			@Override
			public void onPageFinished(WebView view, String url) {

				// This method will be executed each time a page finished loading. The only we do is dismiss the progressDialog, in case we
				// are showing any.
				if (progressDialog != null && progressDialog.isShowing()) {

					progressDialog.dismiss();
					platformDialog.show();

				}

			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String authorizationUrl) {

				boolean shouldOverride = false;

				if (selectedPlatform.equals(ApplicationSettings.PLATFORM_LINKEDIN)) {

					shouldOverride = shouldOverridelinkedInUrlLoading(authorizationUrl);

					if (closeDialog) {

						platformDialog.cancel();
						closeDialog = false;

					}

					return shouldOverride;

				} else if (selectedPlatform.equals(ApplicationSettings.PLATFORM_FACEBOOK)) {

					shouldOverride = shouldOverrideFacebookUrlLoading(authorizationUrl);

					if (closeDialog) {

						platformDialog.cancel();
						closeDialog = false;

					}

					return shouldOverride;

				}

				return false;

			}

		});

		platformDialog.setView(platformWebView);

	}

	/**
	 * When user leaves activity, widget is configured and refreshed with new settings.
	 */
	@Override
	protected void onStop() {

		Logger.log(Logger.TYPE_INFO, "Widget reloads with new settings.", getClass().getName());

		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
		ComponentName appWidget = new ComponentName(this.getPackageName(), StigmaticWidgetProvider.class.getName());
		int[] appWidgetIds = appWidgetManager.getAppWidgetIds(appWidget);

		Intent configureIntent = new Intent(this, StigmaticWidgetProvider.class);
		configureIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
		configureIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
		configureIntent.putExtra(StigmaticWidgetProvider.CONFIGURE_WIDGET, true);

		// sends broadcast to widget
		sendBroadcast(configureIntent);

		super.onStop();

	}

	/**
	 * Inflates action bar items
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.stigmatic_settings_actionbar, menu);
		return super.onCreateOptionsMenu(menu);

	}

	/**
	 * Event for action bar item selection
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		// Handle presses on the action bar items
		switch (item.getItemId()) {

		case R.id.action_help:
			openTutorialVideo();
			return true;
		case R.id.action_share:
			shareApplication();
			return true;
		default:
			return super.onOptionsItemSelected(item);

		}

	}

	/**
	 * Opens video tutorial when user touches the help button in the action bar
	 */
	private void openTutorialVideo() {

		Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.tutorial);

		MediaController mc = new MediaController(this);

		videoView.setMediaController(mc);

		videoView.setVideoURI(uri);
		// HELLO
		videoDialog.show();
		videoView.start();

	}

	/**
	 * Creates application sharing intent when user touches share button in the action bar
	 */
	private void shareApplication() {

		Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
		sharingIntent.setType("text/plain");
		sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.shareSubject));
		sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, getResources().getString(R.string.shareBody));
		startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.shareVia)));

	}

	private boolean shouldOverridelinkedInUrlLoading(String authorizationUrl) {

		// This method will be called when the Auth proccess redirect to our
		// RedirectUri. We will check the url looking for our RedirectUri
		if (authorizationUrl.startsWith(LinkedInApi.Config.REDIRECT_URI)) {

			Logger.log(Logger.TYPE_INFO, "LinkedIn authorization starts.", getClass().getName());
			Uri uri = Uri.parse(authorizationUrl);

			// We take from the url the authorizationToken and the state token. We have to check that the state token returned by the
			// Service is the same we sent. If not, that means the request may be a result of CSRF and must be rejected.
			String stateToken = uri.getQueryParameter(LinkedInApi.Config.STATE_PARAM);

			if (stateToken == null || !stateToken.equals(LinkedInApi.Config.STATE)) {

				Logger.log(Logger.TYPE_WARN, "LinkedIn authorization failed. State token does not match.", getClass().getName());
				closeDialog = true;
				return true;

			}

			// If the user doesn't allow authorization to our application, the
			// authorizationToken Will be null.
			String authorizationToken = uri.getQueryParameter(LinkedInApi.Config.RESPONSE_TYPE_VALUE);

			if (authorizationToken == null) {

				Logger.log(Logger.TYPE_WARN, "LinkedIn authorization failed. The user doesn't allow authorization.", getClass().getName());
				closeDialog = true;
				return true;

			}

			Logger.log(Logger.TYPE_INFO, "LinkedIn authorization token received.", getClass().getName());

			// Generate URL for requesting Access Token
			String accessTokenUrl = LinkedInApi.getInstance().getAccessTokenUrl(authorizationToken);

			// We make the post request to get access token
			new PostTask(new HttpRequestModel(accessTokenUrl), new IRestTaskCallback() {

				@Override
				public void onTaskComplete(HttpResponseModel responseModel) {

					if (responseModel.getStatusBoolean()) {

						InputStream in = responseModel.getResponse();

						// Convert the string result to a JSON Object
						JSONObject resultJson = null;

						try {

							resultJson = new JSONObject(ConvertUtils.getStringFromInputStream(in));

							// Extract data from JSON Response
							int expiresIn = resultJson.has("expires_in") ? resultJson.getInt("expires_in") : 0;

							String accessToken = resultJson.has("access_token") ? resultJson.getString("access_token") : null;

							Logger.log(Logger.TYPE_INFO, "LinkedIn access token: " + accessToken, getClass().getName());

							if (expiresIn > 0 && accessToken != null) {

								Logger.log(Logger.TYPE_INFO, "LinkedIn access token: " + accessToken + ". It will expires in " + expiresIn
										+ " secs" + accessToken, getClass().getName());

								// Calculate date of expiration
								Calendar calendar = Calendar.getInstance();
								calendar.add(Calendar.SECOND, expiresIn);
								long expireDate = calendar.getTimeInMillis();

								// Store both expires in and access token in shared preferences
								SharedPreferences preferences = getSharedPreferences(ApplicationSettings.SHARED_PREFERENCES_NAME,
										MODE_PRIVATE);
								SharedPreferences.Editor editor = preferences.edit();
								editor.putLong(ApplicationSettings.PLATFORM_LINKEDIN + ApplicationSettings.KEY_EXPIRES, expireDate);
								editor.putString(ApplicationSettings.PLATFORM_LINKEDIN + ApplicationSettings.KEY_ACCESS_TOKEN, accessToken);
								editor.commit();

								lastClickedPlatformStatusImageView.setImageResource(R.drawable.online);

								editor.putBoolean(selectedPlatform, true);

								editor.commit();

							}

						} catch (JSONException e) {

							Logger.log(Logger.TYPE_ERROR, e.getMessage(), getClass().getName());

						}

						Toast.makeText(StigmaticActivity.this,
								getResources().getString(R.string.loginSuccess, getResources().getString(R.string.linkedin)),
								Toast.LENGTH_SHORT).show();

						platformDialog.cancel();

					}

				}

			}).execute();

			return true;

		} else {

			// Default behaviour
			Logger.log(Logger.TYPE_INFO, "Redirecting to: " + authorizationUrl, getClass().getName());

			// Opens browser for other things such as password recovery or sign up
			Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(authorizationUrl));
			startActivity(i);

			return true;

		}

	}

	private boolean shouldOverrideFacebookUrlLoading(String authorizationUrl) {

		// This method will be called when the Auth proccess redirect to our
		// RedirectUri. We will check the url looking for our RedirectUri
		if (authorizationUrl.startsWith(FacebookApi.Config.REDIRECT_URI)) {

			Logger.log(Logger.TYPE_INFO, "Facebook authorization starts.", getClass().getName());
			Uri uri = Uri.parse(authorizationUrl);

			// We take from the url the authorizationToken and the state token. We have to check that the state token returned by the
			// Service is the same we sent. If not, that means the request may be a result of CSRF and must be rejected.
			String stateToken = uri.getQueryParameter(FacebookApi.Config.STATE_PARAM);

			if (stateToken == null || !stateToken.equals(FacebookApi.Config.STATE)) {

				Logger.log(Logger.TYPE_WARN, "Facebook authorization failed. State token does not match.", getClass().getName());
				closeDialog = true;
				return true;

			}

			// If the user doesn't allow authorization to our application, the
			// authorizationToken Will be null.
			String authorizationToken = uri.getQueryParameter(FacebookApi.Config.RESPONSE_TYPE_VALUE);

			if (authorizationToken == null) {

				Logger.log(Logger.TYPE_WARN, "Facebook authorization failed. The user doesn't allow authorization.", getClass().getName());
				closeDialog = true;
				return true;

			}

			Logger.log(Logger.TYPE_INFO, "Facebook authorization token received.", getClass().getName());

			// Generate URL for requesting Access Token
			String accessTokenUrl = FacebookApi.getAccessTokenUrl(authorizationToken);

			Logger.log(Logger.TYPE_INFO, "Facebook access token url: " + accessTokenUrl, getClass().getName());

			// We make the post request to get access token
			new PostTask(new HttpRequestModel(accessTokenUrl), new IRestTaskCallback() {

				@Override
				public void onTaskComplete(HttpResponseModel responseModel) {

					if (responseModel.getStatusBoolean()) {

						InputStream in = responseModel.getResponse();

						// Convert the string result to a JSON Object
						try {

							// Log.e("Token", "" + Utils.getStringFromInputStream(in));
							// resultJson = new JSONObject(Utils.getStringFromInputStream(in));
							String responseString = ConvertUtils.getStringFromInputStream(in);
							String[] response = responseString.split("&");
							String accessToken = response[0].split("=")[1];
							int expiresIn = Integer.parseInt(response[1].split("=")[1]);
							// Extract data from JSON Response
							// int expiresIn = resultJson.has("expires") ? resultJson.getInt("expires") : 0;

							// String accessToken = resultJson.has("access_token") ? resultJson.getString("access_token") : null;

							if (expiresIn > 0 && accessToken != null) {

								Logger.log(Logger.TYPE_INFO, "Facebook access token: " + accessToken + ". It will expires in " + expiresIn
										+ " secs" + accessToken, getClass().getName());

								// Calculate date of expiration
								Calendar calendar = Calendar.getInstance();
								calendar.add(Calendar.SECOND, expiresIn);
								long expireDate = calendar.getTimeInMillis();

								// Store both expires in and access token in shared preferences
								SharedPreferences preferences = getSharedPreferences(ApplicationSettings.SHARED_PREFERENCES_NAME,
										MODE_PRIVATE);
								SharedPreferences.Editor editor = preferences.edit();
								editor.putLong(ApplicationSettings.PLATFORM_FACEBOOK + ApplicationSettings.KEY_EXPIRES, expireDate);
								editor.putString(ApplicationSettings.PLATFORM_FACEBOOK + ApplicationSettings.KEY_ACCESS_TOKEN, accessToken);
								editor.commit();

								lastClickedPlatformStatusImageView.setImageResource(R.drawable.online);

								editor.putBoolean(selectedPlatform, true);
								editor.commit();

							}

						} catch (Exception e) {

							Logger.log(Logger.TYPE_ERROR, e.getMessage(), getClass().getName());

						}

						Toast.makeText(StigmaticActivity.this,
								getResources().getString(R.string.loginSuccess, getResources().getString(R.string.facebook)),
								Toast.LENGTH_SHORT).show();

						platformDialog.cancel();

					}

				}

			}).execute();

			return true;

		} else if (authorizationUrl.startsWith(FacebookApi.Config.LOGIN_URL)
				|| authorizationUrl.startsWith(FacebookApi.Config.AUTHORIZATION_URL)
				|| authorizationUrl.startsWith(FacebookApi.Config.CHECKPOINT_URL)) {

			Logger.log(Logger.TYPE_INFO, "Login to: " + authorizationUrl, getClass().getName());
			return false;

		} else {

			// Default behaviour
			Logger.log(Logger.TYPE_INFO, "Redirecting to: " + authorizationUrl, getClass().getName());

			// Opens browser for other things such as password recovery or sign up
			Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(authorizationUrl));
			startActivity(i);

			return true;

		}

	}

	/**
	 * In case user clicks a platform, onClick event is fired
	 */
	private OnClickListener settingsItemClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			selectedPlatform = v.getTag().toString();

			SharedPreferences prefs = getSharedPreferences(ApplicationSettings.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
			final SharedPreferences.Editor editor = prefs.edit();

			String accessToken = prefs.getString(selectedPlatform + ApplicationSettings.KEY_ACCESS_TOKEN, null);
			ImageView statusImageView = (ImageView) v.findViewById(R.id.statusImageView);
			boolean platformSelected = prefs.getBoolean(selectedPlatform, false);

			if (platformSelected) {

				statusImageView.setImageResource(R.drawable.offline);
				editor.putBoolean(selectedPlatform, false);
				editor.putLong(selectedPlatform + ApplicationSettings.KEY_TIMESTAMP, 0);

				if (!selectedPlatform.equals(ApplicationSettings.PLATFORM_CALL)
						&& !selectedPlatform.equals(ApplicationSettings.PLATFORM_SMS)) {

					AsyncTask<Long, Object, Object> deletePlatformItemsTask = new AsyncTask<Long, Object, Object>() {

						@Override
						protected Object doInBackground(Long... params) {

							DatabaseManager.getInstance(getApplicationContext()).deleteSelectedPlatformItems(selectedPlatform);
							return null;

						}

					};

					deletePlatformItemsTask.execute();

				}

				if (selectedPlatform.equals(ApplicationSettings.PLATFORM_LINKEDIN)) {

					Toast.makeText(StigmaticActivity.this,
							getResources().getString(R.string.logoutSuccess, getResources().getString(R.string.linkedin)),
							Toast.LENGTH_SHORT).show();
					editor.putString(ApplicationSettings.PLATFORM_LINKEDIN + ApplicationSettings.KEY_ACCESS_TOKEN, null);

					LinkedInApi.getInstance().logout(accessToken, new IGetCallback() {

						@Override
						public void onDataReceived(HttpResponseModel response) {

							// just doing nothing.. this is an extra step

						}

					});

				} else if (selectedPlatform.equals(ApplicationSettings.PLATFORM_FACEBOOK)) {

					Toast.makeText(StigmaticActivity.this,
							getResources().getString(R.string.logoutSuccess, getResources().getString(R.string.facebook)),
							Toast.LENGTH_SHORT).show();
					editor.putString(ApplicationSettings.PLATFORM_FACEBOOK + ApplicationSettings.KEY_ACCESS_TOKEN, null);

					// Logoutu buraya yapabilirsin yukardaki gibi..

				}

				editor.commit();

			} else {

				// login needed
				if (selectedPlatform != ApplicationSettings.PLATFORM_SMS && selectedPlatform != ApplicationSettings.PLATFORM_CALL
						&& accessToken == null) {

					String authUrl = null;

					if (selectedPlatform.equals(ApplicationSettings.PLATFORM_LINKEDIN)) {

						// Get the LinkedIn authorization Url
						authUrl = LinkedInApi.getInstance().getAuthorizationUrl();

					} else if (selectedPlatform.equals(ApplicationSettings.PLATFORM_FACEBOOK)) {

						// Get the Facebook authorization Url
						authUrl = FacebookApi.getAuthorizationUrl();

					}

					lastClickedPlatformStatusImageView = statusImageView;

					Logger.log(Logger.TYPE_INFO, "Loading Auth Url: " + authUrl, getClass().getName());

					// TODO this controll will be removed after all platforms
					// set
					if (authUrl != null) {

						progressDialog = ProgressDialog.show(StigmaticActivity.this, "", getResources().getString(R.string.loading), true);

						// Load the authorization URL into the webView
						platformWebView.loadUrl(authUrl);

					}

				} else {

					statusImageView.setImageResource(R.drawable.online);
					editor.putBoolean(selectedPlatform, true);
					editor.commit();

				}

			}

		}

	};

	/**
	 * Tab selection event for fragments. It fires when user touches any tab.
	 */
	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {

		currentTabIndex = tab.getPosition();

		// selection
		viewPager.setCurrentItem(tab.getPosition());

		if (tab.getPosition() == ApplicationSettings.INDEX_FRAGMENT_SETTINGS) {

			setSettingsFragmentViews();

		}

	}

	/**
	 * Initializes screen
	 */
	private void initScreen() {

		SharedPreferences prefs = getSharedPreferences(ApplicationSettings.SHARED_PREFERENCES_NAME, MODE_PRIVATE);

		LinearLayout linearLayout = null;
		ImageView platformImageView = null;
		ImageView statusImageView = null;
		TextView platformTextView = null;

		// Facebook
		linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.stigmatic_settings_item_layout, null);

		platformImageView = (ImageView) linearLayout.findViewById(R.id.platformImageView);
		platformImageView.setImageResource(R.drawable.facebook);
		platformTextView = (TextView) linearLayout.findViewById(R.id.platformTextView);
		platformTextView.setText(getResources().getString(R.string.facebook));
		statusImageView = (ImageView) linearLayout.findViewById(R.id.statusImageView);

		linearLayout.setTag(ApplicationSettings.PLATFORM_FACEBOOK);

		if (prefs.getBoolean(ApplicationSettings.PLATFORM_FACEBOOK, false)) {

			statusImageView.setImageResource(R.drawable.online);

		} else {

			statusImageView.setImageResource(R.drawable.offline);

		}

		linearLayout.setOnClickListener(settingsItemClickListener);

		settingsTableLayout.addView(linearLayout);
		settingsTableLayout.addView((View) getLayoutInflater().inflate(R.layout.stigmatic_settings_separator, null));

		// LinkedIn
		linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.stigmatic_settings_item_layout, null);

		platformImageView = (ImageView) linearLayout.findViewById(R.id.platformImageView);
		platformImageView.setImageResource(R.drawable.linkedin);
		platformTextView = (TextView) linearLayout.findViewById(R.id.platformTextView);
		platformTextView.setText(getResources().getString(R.string.linkedin));
		statusImageView = (ImageView) linearLayout.findViewById(R.id.statusImageView);

		linearLayout.setTag(ApplicationSettings.PLATFORM_LINKEDIN);

		if (prefs.getBoolean(ApplicationSettings.PLATFORM_LINKEDIN, false)) {

			statusImageView.setImageResource(R.drawable.online);

		} else {

			statusImageView.setImageResource(R.drawable.offline);

		}

		linearLayout.setOnClickListener(settingsItemClickListener);

		settingsTableLayout.addView(linearLayout);
		settingsTableLayout.addView((View) getLayoutInflater().inflate(R.layout.stigmatic_settings_separator, null));

		// Sms
		linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.stigmatic_settings_item_layout, null);

		platformImageView = (ImageView) linearLayout.findViewById(R.id.platformImageView);
		platformImageView.setImageResource(R.drawable.sms);
		platformTextView = (TextView) linearLayout.findViewById(R.id.platformTextView);
		platformTextView.setText(getResources().getString(R.string.sms));
		statusImageView = (ImageView) linearLayout.findViewById(R.id.statusImageView);

		linearLayout.setTag(ApplicationSettings.PLATFORM_SMS);

		if (prefs.getBoolean(ApplicationSettings.PLATFORM_SMS, false)) {

			statusImageView.setImageResource(R.drawable.online);

		} else {

			statusImageView.setImageResource(R.drawable.offline);

		}

		linearLayout.setOnClickListener(settingsItemClickListener);

		settingsTableLayout.addView(linearLayout);
		settingsTableLayout.addView((View) getLayoutInflater().inflate(R.layout.stigmatic_settings_separator, null));

		// Call
		linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.stigmatic_settings_item_layout, null);

		platformImageView = (ImageView) linearLayout.findViewById(R.id.platformImageView);
		platformImageView.setImageResource(R.drawable.call);
		platformTextView = (TextView) linearLayout.findViewById(R.id.platformTextView);
		platformTextView.setText(getResources().getString(R.string.call));
		statusImageView = (ImageView) linearLayout.findViewById(R.id.statusImageView);

		linearLayout.setTag(ApplicationSettings.PLATFORM_CALL);

		if (prefs.getBoolean(ApplicationSettings.PLATFORM_CALL, false)) {

			statusImageView.setImageResource(R.drawable.online);

		} else {

			statusImageView.setImageResource(R.drawable.offline);

		}

		linearLayout.setOnClickListener(settingsItemClickListener);

		settingsTableLayout.addView(linearLayout);

		linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.stigmatic_settings_advanced_layout, null);

		settingsTableLayout.addView(linearLayout);

		Button cleanCacheButton = (Button) linearLayout.findViewById(R.id.cleanCacheButton);
		cleanCacheButton.setOnClickListener(cleanCacheButtonClickListener);

		Spinner syncPeriodSpinner = (Spinner) linearLayout.findViewById(R.id.syncPeriodSpinner);

		List<String> list = new ArrayList<String>();
		list.add(ApplicationSettings.TIME_LABEL_THREE_HOURS);
		list.add(ApplicationSettings.TIME_LABEL_SIX_HOURS);
		list.add(ApplicationSettings.TIME_LABEL_TWELVE_HOURS);
		list.add(ApplicationSettings.TIME_LABEL_ONE_DAY);

		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		syncPeriodSpinner.setAdapter(dataAdapter);
		syncPeriodSpinner.setOnItemSelectedListener(syncPeriodSpinnerItemSelectedListener);

		long selectedPeriodTime = prefs.getLong(ApplicationSettings.KEY_SYNC_PERIOD, ApplicationSettings.TIME_ONE_DAY);

		if (selectedPeriodTime == ApplicationSettings.TIME_THREE_HOURS)
			syncPeriodSpinner.setSelection(0);
		else if (selectedPeriodTime == ApplicationSettings.TIME_SIX_HOURS)
			syncPeriodSpinner.setSelection(1);
		else if (selectedPeriodTime == ApplicationSettings.TIME_TWELVE_HOURS)
			syncPeriodSpinner.setSelection(2);
		else if (selectedPeriodTime == ApplicationSettings.TIME_ONE_DAY)
			syncPeriodSpinner.setSelection(3);

		Spinner updateIntervalSpinner = (Spinner) linearLayout.findViewById(R.id.updateIntervalSpinner);

		list = new ArrayList<String>();
		list.add(ApplicationSettings.TIME_LABEL_FIFTEEN_MINUTES);
		list.add(ApplicationSettings.TIME_LABEL_THIRTY_MINUTES);
		list.add(ApplicationSettings.TIME_LABEL_ONE_HOUR);
		list.add(ApplicationSettings.TIME_LABEL_TWO_HOURS);
		list.add(ApplicationSettings.TIME_LABEL_THREE_HOURS);
		list.add(ApplicationSettings.TIME_LABEL_SIX_HOURS);
		list.add(ApplicationSettings.TIME_LABEL_TWELVE_HOURS);
		list.add(ApplicationSettings.TIME_LABEL_ONE_DAY);

		dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		updateIntervalSpinner.setAdapter(dataAdapter);
		updateIntervalSpinner.setOnItemSelectedListener(updateIntervalSpinnerItemSelectedListener);

		long selectedUpdateIntervalTime = prefs.getLong(ApplicationSettings.KEY_UPDATE_INTERVAL, ApplicationSettings.TIME_THIRTY_MINUTES);

		if (selectedUpdateIntervalTime == ApplicationSettings.TIME_FIFTEEN_MINUTES)
			updateIntervalSpinner.setSelection(0);
		else if (selectedUpdateIntervalTime == ApplicationSettings.TIME_THIRTY_MINUTES)
			updateIntervalSpinner.setSelection(1);
		else if (selectedUpdateIntervalTime == ApplicationSettings.TIME_ONE_HOUR)
			updateIntervalSpinner.setSelection(2);
		else if (selectedUpdateIntervalTime == ApplicationSettings.TIME_TWO_HOURS)
			updateIntervalSpinner.setSelection(3);
		else if (selectedUpdateIntervalTime == ApplicationSettings.TIME_THREE_HOURS)
			updateIntervalSpinner.setSelection(4);
		else if (selectedUpdateIntervalTime == ApplicationSettings.TIME_SIX_HOURS)
			updateIntervalSpinner.setSelection(5);
		else if (selectedUpdateIntervalTime == ApplicationSettings.TIME_TWELVE_HOURS)
			updateIntervalSpinner.setSelection(6);
		else if (selectedUpdateIntervalTime == ApplicationSettings.TIME_ONE_DAY)
			updateIntervalSpinner.setSelection(7);

	}

	/**
	 * Widget sync period spinner selection event is handled here
	 */
	private OnItemSelectedListener syncPeriodSpinnerItemSelectedListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

			String selectedPeriod = parent.getItemAtPosition(pos).toString();
			Logger.log(Logger.TYPE_INFO, "Selected sync period: " + selectedPeriod, getClass().getName());

			SharedPreferences preferences = getSharedPreferences(ApplicationSettings.SHARED_PREFERENCES_NAME, MODE_MULTI_PROCESS);
			SharedPreferences.Editor editor = preferences.edit();

			long selectedPeriodTime = 0;

			if (selectedPeriod.equals(ApplicationSettings.TIME_LABEL_THREE_HOURS))
				selectedPeriodTime = ApplicationSettings.TIME_THREE_HOURS;
			else if (selectedPeriod.equals(ApplicationSettings.TIME_LABEL_SIX_HOURS))
				selectedPeriodTime = ApplicationSettings.TIME_SIX_HOURS;
			else if (selectedPeriod.equals(ApplicationSettings.TIME_LABEL_TWELVE_HOURS))
				selectedPeriodTime = ApplicationSettings.TIME_TWELVE_HOURS;
			else if (selectedPeriod.equals(ApplicationSettings.TIME_LABEL_ONE_DAY))
				selectedPeriodTime = ApplicationSettings.TIME_ONE_DAY;

			editor.putLong(ApplicationSettings.KEY_SYNC_PERIOD, selectedPeriodTime);
			editor.commit();

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {

			// TODO Auto-generated method stub

		}

	};

	/**
	 * Widget update inverval spinner selection event is handled here
	 */
	private OnItemSelectedListener updateIntervalSpinnerItemSelectedListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

			String selectedUpdateInterval = parent.getItemAtPosition(pos).toString();
			Logger.log(Logger.TYPE_INFO, "Selected update interval: " + selectedUpdateInterval, getClass().getName());

			SharedPreferences preferences = getSharedPreferences(ApplicationSettings.SHARED_PREFERENCES_NAME, MODE_MULTI_PROCESS);
			SharedPreferences.Editor editor = preferences.edit();

			long selectedUpdateTime = 0;

			if (selectedUpdateInterval.equals(ApplicationSettings.TIME_LABEL_FIFTEEN_MINUTES))
				selectedUpdateTime = ApplicationSettings.TIME_FIFTEEN_MINUTES;
			else if (selectedUpdateInterval.equals(ApplicationSettings.TIME_LABEL_THIRTY_MINUTES))
				selectedUpdateTime = ApplicationSettings.TIME_THIRTY_MINUTES;
			else if (selectedUpdateInterval.equals(ApplicationSettings.TIME_LABEL_ONE_HOUR))
				selectedUpdateTime = ApplicationSettings.TIME_ONE_HOUR;
			else if (selectedUpdateInterval.equals(ApplicationSettings.TIME_LABEL_TWO_HOURS))
				selectedUpdateTime = ApplicationSettings.TIME_TWO_HOURS;
			else if (selectedUpdateInterval.equals(ApplicationSettings.TIME_LABEL_THREE_HOURS))
				selectedUpdateTime = ApplicationSettings.TIME_THREE_HOURS;
			else if (selectedUpdateInterval.equals(ApplicationSettings.TIME_LABEL_SIX_HOURS))
				selectedUpdateTime = ApplicationSettings.TIME_SIX_HOURS;
			else if (selectedUpdateInterval.equals(ApplicationSettings.TIME_LABEL_TWELVE_HOURS))
				selectedUpdateTime = ApplicationSettings.TIME_TWELVE_HOURS;
			else if (selectedUpdateInterval.equals(ApplicationSettings.TIME_LABEL_ONE_DAY))
				selectedUpdateTime = ApplicationSettings.TIME_ONE_DAY;

			editor.putLong(ApplicationSettings.KEY_UPDATE_INTERVAL, selectedUpdateTime);
			editor.commit();

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {

			// TODO Auto-generated method stub

		}

	};

	/**
	 * In case user clicks a clean cache button, we just remove all timestamps and clean the database
	 */
	private OnClickListener cleanCacheButtonClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			SharedPreferences preferences = getSharedPreferences(ApplicationSettings.SHARED_PREFERENCES_NAME, Context.MODE_MULTI_PROCESS);

			SharedPreferences.Editor editor = preferences.edit();
			editor.remove(ApplicationSettings.PLATFORM_FACEBOOK + ApplicationSettings.KEY_TIMESTAMP);
			editor.remove(ApplicationSettings.PLATFORM_LINKEDIN + ApplicationSettings.KEY_TIMESTAMP);
			editor.commit();

			AsyncTask<Long, Object, Object> deleteTask = new AsyncTask<Long, Object, Object>() {

				@Override
				protected Object doInBackground(Long... params) {

					DatabaseManager.getInstance(getApplicationContext()).deleteEntireTable();
					return null;

				}

				protected void onPostExecute(Object result) {

					Toast.makeText(StigmaticActivity.this, getResources().getString(R.string.clearCacheMessage), Toast.LENGTH_SHORT).show();

				};

			};

			deleteTask.execute();

		}

	};

	/**
	 * Used for internal caching mechanism on tab change
	 */
	public void setSettingsFragmentViews() {

		if (settingsTableLayout == null) {

			SettingsFragment settingsFragment = (SettingsFragment) tabPagerAdapter.getItem(currentTabIndex);

			if (pageInit) {

				settingsTableLayout = settingsFragment.getSettingsTableLayout();

				if (settingsTableLayout != null && settingsTableLayout.getChildCount() <= 0) {

					initScreen();

				}

			} else {

				pageInit = true;
				settingsFragment.setPageInit(true);

			}

		}

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
	}

}